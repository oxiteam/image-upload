<?php
require('../inc/config.php');

$User = new Users();

$user_id = abs(intval($_REQUEST['user_id']));
$page_no = abs(intval($_REQUEST['page_no']));

$userInfo = $User -> fetch_user_by_id($user_id);

$smarty->assign('user',$userInfo);
$smarty->assign('page_no',$page_no);

$modal_body =  $smarty->fetch($tpl_folder.'/admin-1000/users-form.tpl');

$json_encoded = json_encode(array('modal_body'=>$modal_body), true);

/* Return JSON */
die($json_encoded);

//exit;
