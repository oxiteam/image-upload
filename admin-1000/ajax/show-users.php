<?php
require('inc/config.php');
//require('../inc/functions.php');

$Users = new Users();
$FormatDate = new FormatDate();
$Sanitation = new Sanitation();

$users_types_list = $Users->fetch_users_types();
$smarty->assign('users_types_list',$users_types_list);

$page= $_REQUEST['pageId']>0?intval($_REQUEST['pageId']):1;
$type = $_REQUEST['type'];

$limit = $config['CT_NUMBER_OF_USERS_PER_PAGE'];

$start = ($page - 1) * $limit;
//var_dump($_REQUEST);

$where = array();
#$get_data = array();
$get_data['s_name'] = $Sanitation->remove_html($_REQUEST['s_name'], true);
$get_data['s_email'] = $Sanitation->remove_html($_REQUEST['s_email'], true);
$get_data['s_ip'] = $Sanitation->remove_html($_REQUEST['s_ip'], true);
$get_data['s_date_start'] = $Sanitation->remove_html($_REQUEST['s_date_start'], true);
$get_data['s_date_end'] = $Sanitation->remove_html($_REQUEST['s_date_end'], true);
$get_data['s_type'] = $Sanitation->remove_html($_REQUEST['s_type'], true);

if ($get_data['s_name'] != '' && $get_data['s_name'] != 'undefined') {
    $where[] = " AND user_name LIKE '%".$Sanitation->prepare_for_database($get_data['s_name'])."%' " ;
}
if ($get_data['s_email'] != '' && $get_data['s_email'] != 'undefined') {
    $where[] = " AND email LIKE '%".$Sanitation->prepare_for_database($get_data['s_email'])."%' " ;
}
if ($get_data['s_ip'] != '' && $get_data['s_ip'] != 'undefined') {
    $where[] = " AND ip_address LIKE '%".$Sanitation->prepare_for_database($get_data['s_ip'])."%' " ;
}
if ($get_data['s_date_start'] != '' && $get_data['s_date_start'] != 'undefined') {
    $where[] = " AND created_date >= '".date("Y-m-d", strtotime($get_data['s_date_start']))." 00:00:00' " ;
}
if ($get_data['s_date_end'] != '' && $get_data['s_date_end'] != 'undefined') {
    $where[] = " AND created_date <= '".date("Y-m-d", strtotime($get_data['s_date_end']))." 23:59:59' " ;
}
if ($get_data['s_type'] != '' && $get_data['s_type'] != 'undefined') {
    $where[] = " AND user_type = '".$Sanitation->prepare_for_database($get_data['s_type'])."' " ;
}

$smarty->assign('s_name',$get_data['s_name']);
$smarty->assign('s_email',$get_data['s_email']);
$smarty->assign('s_ip',$get_data['s_ip'] );
$smarty->assign('s_date_start',$get_data['s_date_start']);
$smarty->assign('s_date_end',$get_data['s_date_end']);

$users_db = $Users -> fetch_users($start,$limit,'created_date','desc', '');
$where[] = " AND user_type = '1' ";
$total_users =  $Users -> count_users(implode(' ', $where));

$usersList = array();
foreach ($users_db as $key => $user)
{
    $usersList[$key] = $user;
    $usersList[$key]['created_date'] =  $FormatDate -> long_format($user['created_date']);
}

//print_r('<pre>');print_r($usersList);print_r('</pre>');

//print_r('<pre>');print_r($total_users);print_r('</pre>');

$nr_of_page=ceil($total_users/$limit);

$min_no = $page - $config['CT__NUMBER_OF_PAGINATIONS_NUMBER'];
$min_page = ($min_no <= 0 )? 1 : $min_no;

$max_no = $page + $config['CT__NUMBER_OF_PAGINATIONS_NUMBER'];
$max_page = ($max_no >= $nr_of_page )? $nr_of_page : $max_no;

if ($type == 'admin')
{
    $link_pagination = $admin_users_url;
    $link_ajax = '!';
}
else
{
    $link_pagination = $admin_users_front_url;
    $link_ajax = '';
}
//print_r('<pre>');print_r($min_page . ' -> '.$max_page);print_r('</pre>');

$smarty->assign('page_no',$page);
$smarty->assign('no_of_page',$nr_of_page);
$smarty->assign('type',$type);
$smarty->assign('current_thread', 0);
$smarty->assign('allUsers',$usersList);
$smarty->assign('link_pagination', $link_pagination);
$smarty->assign('link_ajax', $link_ajax);
$smarty->assign('min_page',$min_page);
$smarty->assign('max_page',$max_page);

if ($type == 'admin') {
    $smarty->display($tpl_folder.'/admin-1000/users_list.tpl');
} else {
  #  $smarty->display($tpl_folder.'admin-1000/users_front_list.tpl');
}
