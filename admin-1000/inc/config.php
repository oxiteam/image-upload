<?php
require(dirname(dirname(dirname(__FILE__))) . '/inc/config.php');

// Remove this in production!
ini_set("display_errors", 1);
error_reporting(E_ALL ^ E_NOTICE);

//Variables
$tpl_folder = $config['BASE_DIR'] . '/tpl/';
$tpl_folder_admin = $config['BASE_DIR'] . 'tpl/admin-1000';
$tpl_folder_root = $config['BASE_DIR'] . '/tpl';
$root_url_admin = $config['BASE_URL'].'/admin-1000';

//url variables
$home_url = $root_url . '/';
$admin_login_url = $root_url_admin.'?action=admin-login';
$admin_logout_url = $root_url_admin.'?action=admin-logout';
$admin_add_admin_url = $root_url_admin.'?action=admin-add_admin';
$admin_edit_admin_url = $root_url_admin.'?action=admin-edit_admin';
$admin_delete_admin_url = $root_url_admin.'?action=admin-delete_admin';
$admin_errors_url = $root_url_admin.'?action=admin-errors';
$admin_assets_url = $root_url_admin.'?action=admin-assets';
$admin_assets_add_url = $root_url_admin.'?action=admin-assets-add';
$admin_assets_edit_url = $root_url_admin.'?action=admin-assets-edit';
$admin_assets_delete_url = $root_url_admin.'?action=admin-assets-delete';
$admin_settings_url = $root_url_admin.'?action=admin-settings';
$admin_emails_management_url = $root_url_admin.'?action=admin-emails_management';

//LOAD CLASSES

require $config['BASE_DIR'] . '/inc/bcrypt.class.php';
require $config['BASE_DIR'] . '/inc/FormatDate.class.php';
require $config['BASE_DIR'] . '/inc/Settings.class.php';
require $config['BASE_DIR'] . '/inc/Site_settings.class.php';
require $config['BASE_DIR'] . '/admin-1000/inc/loginAdmin.class.php';
require $config['BASE_DIR'] . '/admin-1000/inc/Users.class.php';
require $config['BASE_DIR'] . '/inc/ErrorHandler.php';
require $config['BASE_DIR'] . '/inc/emails_management.class.php';

//Smarty Assigns
$smarty->assign("root_url_admin", $root_url_admin);
$smarty->assign("admin_login_url", $admin_login_url);
$smarty->assign("admin_logout_url", $admin_logout_url);
$smarty->assign("admin_add_admin_url", $admin_add_admin_url);
$smarty->assign("admin_edit_admin_url", $admin_edit_admin_url);
$smarty->assign("admin_delete_admin_url", $admin_delete_admin_url);
$smarty->assign("admin_errors_url", $admin_errors_url);
$smarty->assign("admin_assets_url", $admin_assets_url);
$smarty->assign("admin_assets_add_url", $admin_assets_add_url);
$smarty->assign("admin_assets_edit_url", $admin_assets_edit_url);
$smarty->assign("admin_assets_delete_url", $admin_assets_delete_url);
$smarty->assign("admin_settings_url", $admin_settings_url);
$smarty->assign('admin_emails_management_url',$admin_emails_management_url );
$smarty->assign('tpl_folder_admin',$tpl_folder_admin );
