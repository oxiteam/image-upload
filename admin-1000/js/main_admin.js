//fix CKEditor in a Bootstrap Modal?
if(typeof $.fn.modal != 'undefined') {
    $.fn.modal.Constructor.prototype.enforceFocus = function () {
        modal_this = this
        $(document).on('focusin.modal', function (e) {
            if (modal_this.$element[0] !== e.target && !modal_this.$element.has(e.target).length
                // add whatever conditions you need here:
                &&
                !$(e.target.parentNode).hasClass('cke_dialog_ui_input_select') && !$(e.target.parentNode).hasClass('cke_dialog_ui_input_text')) {
                modal_this.$element.focus()
            }
        })
    };
}

$(document).ready(function()
{
    $('#admin_top_menu li').on('click', function(e)
    {
        e.preventDefault();
        urlString = $(this).find("a").attr('href');
        //check if li a has an url in href
        if (urlString.indexOf('http://') === 0 || urlString.indexOf('https://') === 0)
        {
            location.href=urlString;
        }
    });
    $('#admin_top_menu li a').on('click', function(e)
    {
        e.preventDefault();
    });


    //find if is need it to toggle articles for a menu or not
    var article_menu_action = getUrlParameter('toggle_action');

    //console.log(article_menu_action);

    if(typeof article_menu_action != 'undefined' &&  article_menu_action == 'toggle_articles')
    {
        hash =  window.location.hash;

        //console.log(hash);

        menu_id=hash.match("[0-9]+");

        //console.log(menu_id);

        if ($("#"+menu_id+"_menu_section").length) {
            $('html,body').animate({
                scrollTop: $("#"+menu_id+"_menu_section").offset().top
            });
        }

        $("#"+menu_id+"_menu_tr_holder span.glyphicon-list-alt").removeClass("disabled");
        $("#"+menu_id+"_articles").hide();
        toggle_articles(menu_id);
        $("#"+menu_id+"_menu_tr_holder span.glyphicon-plus-sign").parent().hide();
    }


    function upload(){
        var fileIn = $("#fileToUpload")[0];
        //Has any file been selected yet?
        if (fileIn.files === undefined || fileIn.files.length == 0) {
            alert("Please select a file");
            return;
        }

        //We will upload only one file in this demo
        var file = fileIn.files[0];

        var data = new FormData();
        $.each($('#fileToUpload')[0].files, function(i, file) {
            data.append('file-'+i, file);
        });

        console.log(data);
        //Show the progress bar
        $("#progressbar").show();
        // return false;

        public_name = $("#public_name").val();
        file_description = $("#file_description").val();
        file_type = $("#file_type").val();
        $.ajax({
            url: "/admin-1000/ajax/assets_uploader.php?public_name=" + public_name + "&file_description=" +file_description + "&file_type=" +file_type,
            type: "POST",
            data: data,
            processData: false, //Work around #1
            cache: false,
            contentType: false,
            dataType: "json",
            success: function(response){
                $("#progressbar").hide();
                if (response.message != 'ok') {
                    $("#asset_error_message").html(response.message).show();
                }
                else
                {
                    if (response.warning != '') {
                        $("#asset_error_message").html(response.warning).show();
                        setTimeout(function () {
                            window.location = "/admin-1000/?action=admin-assets";
                        }, 4000);
                    }
                    else
                    {
                        window.location = "/admin-1000/?action=admin-assets";
                    }

                }
                console.log(response.message);
                //
            },
            error: function(jqXHR, textStatus, errorThrown) {
                //alert("Failed");
                $("#asset_error_message").html(errorThrown).show();
            },
            //Work around #3
            xhr: function() {
                myXhr = $.ajaxSettings.xhr();
                if(myXhr.upload){
                    myXhr.upload.addEventListener('progress',showProgress, false);
                } else {
                    console.log("Uploadress is not supported.");
                }
                return myXhr;
            }
        });

        return false;
    }

    function showProgress(evt) {
        if (evt.lengthComputable) {
            var percentComplete = (evt.loaded / evt.total) * 100;
            $('#progressbar').progressbar("option", "value", percentComplete );
        }
    }

    if ($("#upload_btn").length > 0) {
        $("#progressbar").progressbar();
        $("#progressbar").hide();
        $("#upload_btn").click(upload);
    }


    if ( $("#icons-legend").length) {
        var newTitle = $("#icons-legend").html();

        $('.tooltip_trigger').tooltip();
        $('.tooltip_trigger').attr('data-original-title', newTitle);


    }

    //check if exist parameter: menu_level and default open that section
    var menu_level = getUrlParameter('menu_level');
    if (menu_level)
    {
        $('#level_'+menu_level+'_menu').show();
    }

    /* Search */
    $(".ic-search").click(function () {
        if($('.filter_holder').hasClass("activ"))
        {
            var action = getUrlParameter('action');
            var url =  window.location.href.replace( /[\?#].*|$/, "" );

           // console.log(url);
            window.location = url+"?action="+action;
        }
        else
        {
            $(".filter_holder").slideToggle("slow");
            $(".filter_holder form").find("input[type=text], textarea, select").val("");
            $(this).toggleClass("activ");
        }

    });

    /* End Search */

    $('#clear_all_trigger').on('click', function(e)
    {
        if (confirm('Esti sigur ca vrei sa stergi toate erorile?'))
        {
            $.ajax({
                url: '/admin-1000/ajax/clear-error.php',
                type: 'GET',
                cache: false,
                success: function(result) {
                    window.location = "/admin-1000/?action=admin-errors";
                }
            });
        }
        else
        {
            return false;
        }

    });

    $('#new_file_trigger').click(function()
    {
        $('#form_files').slideToggle();
    });
});

function check_url()
{
    var url = window.location.href;

    //check if  hash exists in url
    var res = url.match("pagina=[0-9]+");

    //if yes,get number from hash
    if(res)
    {
        var hash = window.location.hash;
        var has_nr=hash.match("[0-9]+");
        return has_nr;
    }
    else
    {
        return 1;
    }
}

function getUrlParameter(sParam)
{
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++)
    {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam)
        {
            return sParameterName[1];
        }
    }
}

/* ADMINISTRATORS sections */

function current_admins()
{
    var result= check_url();

    if(result!="" || result!='undefined' || typeof(result)!='undefined')
    {
        //result= parseInt(result);
        var prev_item =  result;
    }
    else
    {
        var prev_item  = 1;
    }

    if (prev_item > 0)
    {
        pagination_users(prev_item,'admin');
    }


}

function pagination_users(page_id,type)
{
    var url = window.location.href;

    //console.log(url);
    //console.log(window.location.hash);
    if (page_id)
    {
        //window.location.hash='pagina='+$("#"+page_id).attr('id');
        if (page_id > 1 ) {
            window.location.hash='pagina='+page_id;
        }

    }
    //get all filter parameters
    var s_name = getUrlParameter('s_name');
    var s_email = getUrlParameter('s_email');
    var s_ip = getUrlParameter('s_ip');
    var s_date_start = getUrlParameter('s_date_start');
    var s_date_end = getUrlParameter('s_date_end');
    var s_type = getUrlParameter('s_type');


    //console.log(window.location.hash);
    // return false;
    $("#loading").show();
    $("#list_results").hide();
    $.ajax({
        type: 'POST',
        url: '/admin-1000/ajax/show-users.php',
        data: 'pageId='+ page_id+"&type="+type+"&s_name="+s_name+"&s_email="+s_email+"&s_ip="+s_ip+"&s_date_start="+s_date_start+"&s_date_end="+s_date_end+"&s_type="+s_type, // admin or regular_user
        success: function(response)
        {
            $("#list_results").html(response);
            //console.log(page_id);
            $(".list_pagination li a").removeClass("curent_item").addClass("pagination_item");
            $(".list_pagination li").removeClass("curent_item");
            $("#"+page_id).removeClass("pagination_item").addClass("curent_item");
            $("#"+page_id+"botom").removeClass("pagination_item").addClass("curent_item");

            $("#loading").hide();
            $("#list_results").show();
        }
    });
}


function prev_users(type)
{
    var result= check_url();

    if(result!="" || result!='undefined' || typeof(result)!='undefined')
    {
        //result= parseInt(result);
        var prev_item =  result-1;
    }
    else
    {
        var prev_item  = 0;
    }

    if (prev_item >= 0)
    {
        var url = window.location.href;
        var sort = url.match("type=[a-z]+");

        if(type)
        {
            switch(type)
            {
                case 'admin':
                case 'users':
                    break;
            }

        }
        else
        {
            type="admin";
        }

        pagination_users(prev_item,type);
    }


}


function next_users(type)
{
    var result= check_url();

    if(result!="" || result!='undefined' || typeof(result)!='undefined')
    {
        result=parseInt(result);
        var next_item =result+1;
        var last_page= $('.last_item').html();
        last_page=parseInt(last_page);

        if(next_item>last_page)
        {
            next_item=last_page;

        }
    }
    else
    {
        var next_item  = 0;
    }
    if (result <= last_page ) {
        var url = window.location.href;
        var sort = url.match("type=[a-z]+");

        if(type)
        {
            switch(type)
            {
                case 'admin':
                case 'users':
                    break;
            }

        }
        else
        {
            type="admin";
        }

        pagination_users(next_item,type);
    }


}

function admin_item(user_id, action_type, event)
{
    event.preventDefault();
    $.ajax({
        url: '/admin-1000/ajax/get_user_form.php?user_id='+user_id+'&action_type='+action_type,
        type: 'GET',
        cache: false,
        dataType:'json',
        success: function(result) {
            if (action_type == 'add')
            {
                $("#userModalLabel").html('Add administrator');
            }
            else
            {
                $("#userModalLabel").html('Edit administrator');
            }
            $("#userModal #frm_modal_btn").removeAttr("onclick");
            $("#userModal #frm_modal_btn").off("click");
            $("#userModal #frm_modal_btn").on("click", function () {
                save_admin('frm_admin');
            });
            $("#userModalBody").html( result.modal_body );
        }
    });
    $('#userModal').modal('show');
}

function save_admin(form_id)
{

    $.ajax({
        url: '/admin-1000/ajax/save_user.php',
        type: 'POST',
        data: $('#'+form_id).serialize(),
        cache: false,
        dataType:'json',
        success: function(result) {
            if (result.response == 'ok') {
                window.location.reload(true);
            }
            else
            {
                $("#userModalBody #error_message").html(result.response );
            }
        }
    });
}
function delete_admin(user_id)
{
    if (user_id == 1)
    {
        alert('First administrator can NOT be deleted!');
        return false;
    }
    if (confirm('Are you sure you want to delete the administrator?'))
    {
        $.ajax({
            url: '/admin-1000/?action=admin-delete_user&user_id='+user_id,
            type: 'GET',
            cache: false,
            success: function(result) {
                window.location = "/admin-1000/?action=admin-users";
            }
        });
    }
    else
    {
        return false;
    }

}

/**/

function pagination_errors(page_id,no_of_page)
{
    window.location.hash='pagina='+page_id;
    $("#loading").show();

    var initial = 0;

    $.ajax({
        type: 'GET',
        url: '/admin-1000/ajax/errors_pagination.php',
        data: 'pageId='+ page_id+"&initial="+initial,
        dataType: "json",
        success: function(response)
        {
            //$("#list_results").after(response);
            console.log(response.errors_results_body);
            //  $("#paginare_bottom").before(response.errors_results_body);

            $('#errors_table > tbody').append(response.errors_results_body);

            no_of_page = response.no_of_page;

            if (page_id != no_of_page)
            {
                next_page = parseInt(page_id) + parseInt(1);
                $("#more_results").attr("onclick", "pagination_errors("+next_page+", "+no_of_page+");");
            }
            else
            {
                $("#more_results_container").html('Acestea sunt toate rezultatele.');
            }


            $("#loading").hide();
            $("#list_results").show();
            // $("#list_more_results").html(response);
        }
    });
}



$(function() {
    $('#categoryModal button').on('click', function(e)
    {
        e.preventDefault();
    });
    $('#categoryModal').on('shown.bs.modal', function () {
        $("#categoryModal input:text").first().focus();
    });

    $('#settingModal button').on('click', function(e)
    {
        e.preventDefault();
    });
    $('#settingModal').on('shown.bs.modal', function () {
        $("#settingModal input:text").first().focus();
    });

    $('#articleModal button').on('click', function(e)
    {
        e.preventDefault();
    });
    $('#articleModal').on('shown.bs.modal', function () {
        $("#articleModal input:text").first().focus();
    });

    $('#userModal button').on('click', function(e)
    {
        e.preventDefault();
    });
    $('#userModal').on('shown.bs.modal', function () {
        $("#userModal input:text").first().focus();
    });
});

//======== START Articles ========

function article_item(article_id, action_type, event)
{
    event.stopPropagation();
    event.preventDefault();

    $("#"+article_id+"_content").hide();
    $.ajax({
        url: '/admin-1000/ajax/get_article_form.php?article_id='+article_id+'&action_type='+action_type,
        type: 'GET',
        cache: false,
        dataType:'json',
        success: function(result) {
            if (action_type == 'add')
            {
                $("#articleModalLabel").html('Add article');
            }
            else
            {
                $("#articleModalLabel").html('Edit article');
            }
            $("#articleModal #frm_modal_btn").removeAttr("onclick");
            $("#articleModal #frm_modal_btn").off("click");
            $("#articleModal #frm_modal_btn").on("click", function () {
                save_article('frm_article');
            });
            $("#articleModalBody").html( result.modal_body );
        }
    });
    $('#articleModal').modal('show');
}

function save_article(form_id)
{
    editor_name = 'version_1';
    var editorText = CKEDITOR.instances[editor_name].getData();
    // alert(editorText);
    $("#"+form_id+" #version_1").val(editorText);

    item_value = $("#"+form_id+" #social_media_image").val();
    var item_value_uploaded = $('#social_media_image_upload')[0].files[0];
    if (typeof item_value_uploaded != 'undefined')
    {
        image_type = item_value_uploaded.type
        if (image_type.toLowerCase().indexOf("image") == -1)
        {
            alert('Please upload an image file (jpg, gif, png)');
            return false;
        }
        if (item_value != '' && item_value_uploaded.name != '') {
            if (confirm('You will replace the file with this new one! Are you sure?'))
            {

            }
            else
            {
                return false;
            }
        }
    }

    var m_data = new FormData();
    m_data.append( 'article_id', $("#"+form_id+" #article_id").val());
    m_data.append( 'page_no', $("#"+form_id+" #page_no").val());
    m_data.append( 'category_id', $("#"+form_id+" #category_id").val());
    m_data.append( 'title', $("#"+form_id+" #title").val());
    m_data.append( 'short_title', $("#"+form_id+" #short_title").val());
    m_data.append( 'meta_description', $("#"+form_id+" #meta_description").val());
    m_data.append( 'version_1', editorText);

    m_data.append( 'is_primary', $('input[name=is_primary]:checked').val());
    m_data.append( 'display_order', $("#"+form_id+" #display_order").val());
    m_data.append( 'status', $("#"+form_id+" #status").val());

    m_data.append( 'social_media_image', $("#"+form_id+" #social_media_image").val());
    m_data.append( 'social_media_image_upload', item_value_uploaded);

    $.ajax({
        url: '/admin-1000/ajax/save_article.php',
        type: 'POST',
        data: m_data,
        cache: false,
        dataType:'json',
        processData: false,
        contentType: false,
        success: function(result) {
            if (result.response == 'ok') {
                window.location.reload(true);
            }
            else
            {
                $("#articleModalBody #error_message").html(result.response );
            }
        }
    });
}

function delete_article_item(article_id, event)
{
    event.stopPropagation();
    event.preventDefault();

    $("#"+article_id+"_content").hide();
    if (confirm('Are you sure you want to delete this article?'))
    {
        $.ajax({
            url: '/admin-1000/ajax/delete-article.php?article_id='+article_id,
            type: 'GET',
            cache: false,
            dataType:'json',
            success: function(result) {
                if (result.response == "ok")
                {
                    window.location.reload(true);
                }
            }
        });
    }
    else
    {
        return false;
    }
}


function revert_article_to_version(article_id, version_no)
{

    if (confirm('Are you sure? This operation cannot be undone.'))
    {
        $.ajax({
            url: '/admin-1000/ajax/revert_article_to_version.php?article_id='+article_id+'&version_no='+version_no,
            type: 'GET',
            cache: false,
            dataType:'json',
            success: function(result) {
                window.location.reload(true);
            }
        });
    }
    else
    {
        return false;
    }

}

function show_article_item(article_id, event)
{
    event.stopPropagation();
    event.preventDefault();

    $("#"+article_id+"_content").hide();
    $('#showArticleModalLabel').attr('article_id', article_id);
    // console.log('article_id '+ article_id);
    $.ajax({
        url: '/admin-1000/ajax/get_article_versions.php?article_id='+article_id,
        type: 'GET',
        cache: false,
        dataType:'json',
        success: function(result) {
            $("#showArticleModalBody").html( result.modal_body );
        }
    });
    $('#showArticleModal').modal('show');
}

function move_article_item(article_id, position, event)
{
    event.stopPropagation();
    event.preventDefault();

    $("#"+article_id+"_content").hide();
    $.ajax({
        url: '/admin-1000/ajax/articles-move.php?position='+position+'&article_id='+article_id,
        type: 'GET',
        cache: false,
        dataType:'json',
        success: function(result) {
            window.location.reload(true);
        }
    });

}

function toggle_content(article_id)
{
    $("#"+article_id+"_content").toggle();
}

function update_article_status(article_id, event)
{

    event.stopPropagation();
    event.preventDefault();

    var current_element = $('#update-status-'+article_id);
    var current_status = current_element.attr('current-status');
    var status, error_class, icon_class, old_error_class, old_icon_class, icon_status_class, old_icon_status_class;

    var icon_status_element = $('#icon-status-'+article_id);

    if (current_status == 1) {
        status = 0;
        error_class = 'text-error';
        icon_class = 'glyphicon-ok';

        old_error_class = 'text-success';
        old_icon_class = 'glyphicon-ok';

        old_icon_status_class = '';
        icon_status_class = 'glyphicon-alert';
    }
    else
    {
        status = 1;
        error_class = 'text-success';
        icon_class = 'glyphicon-ok';

        old_error_class = 'text-error';
        old_icon_class = 'glyphicon-ok';

        old_icon_status_class = 'glyphicon-alert';
        icon_status_class = '';

    }

    $.ajax({
        url: '/admin-1000/ajax/update-article-status.php?article_id='+article_id+'&action=update&status='+status,
        type: 'GET',
        cache: false,
        dataType:'json',
        success: function(result) {
            if (result.response == 'ok') {
                current_element.attr('current-status', status);
                current_element.removeClass(old_error_class).addClass(error_class);
                current_element.find('span').removeClass(old_icon_class).addClass(icon_class);

                if (status == 0)
                {
                    icon_status_element.show();
                }
                else
                {
                    icon_status_element.hide();
                }
                icon_status_element.removeClass(old_icon_status_class).addClass(icon_status_class);
                icon_status_element.removeClass(old_error_class).addClass(error_class);
            }
        }
    });


}
function current_articles()
{
    var result= check_url();

    if(result!="" || result!='undefined' || typeof(result)!='undefined')
    {
        //result= parseInt(result);
        var prev_item =  result;
    }
    else
    {
        var prev_item  = 1;
    }

    if (prev_item > 0)
    {
        pagination_articles(prev_item);
    }


}

function pagination_articles(page_id)
{
    var url = window.location.href;

    //console.log(url);
    //console.log(window.location.hash);
    if (page_id)
    {
        //window.location.hash='pagina='+$("#"+page_id).attr('id');
        window.location.hash='pagina='+page_id;

    }
    //get all filter parameters
    var s_title = getUrlParameter('s_title');
    var s_date_start = getUrlParameter('s_date_start');
    var s_date_end = getUrlParameter('s_date_end');


    //console.log(window.location.hash);
    // return false;
    $("#loading").show();
    $("#list_results").hide();
    $.ajax({
        type: 'POST',
        url: '/admin-1000/ajax/show-articles.php',
        data: 'pageId='+ page_id+"&s_title="+s_title+"&s_date_start="+s_date_start+"&s_date_end="+s_date_end,
        success: function(response)
        {
            $("#list_results").html(response);
            //console.log(page_id);
            $(".list_pagination li a").removeClass("curent_item").addClass("pagination_item");
            $(".list_pagination li").removeClass("curent_item");
            $("#"+page_id).removeClass("pagination_item").addClass("curent_item");
            $("#"+page_id+"botom").removeClass("pagination_item").addClass("curent_item");

            $("#loading").hide();
            $("#list_results").show();

        }
    });
}


function prev_articles()
{
    var result= check_url();

    if(result!="" || result!='undefined' || typeof(result)!='undefined')
    {
        //result= parseInt(result);
        var prev_item =  result-1;
    }
    else
    {
        var prev_item  = 0;
    }

    if (prev_item >= 0)
    {
        pagination_articles(prev_item);
    }


}


function next_articles()
{
    var result= check_url();

    if(result!="" || result!='undefined' || typeof(result)!='undefined')
    {
        result=parseInt(result);
        var next_item =result+1;
        var last_page= $('.last_item').html();
        last_page=parseInt(last_page);

        if(next_item>last_page)
        {
            next_item=last_page;

        }
    }
    else
    {
        var next_item  = 0;
    }
    if (result <= last_page ) {
        pagination_articles(next_item);
    }


}

//======== END Articles ========



/*START ASSETS ACTIONS */

function current_assets()
{
    var result= check_url();

    if(result!="" || result!='undefined' || typeof(result)!='undefined')
    {
        //result= parseInt(result);
        var prev_item =  result;
    }
    else
    {
        var prev_item  = 1;
    }

    if (prev_item > 0)
    {
        pagination_assets(prev_item);
    }

}
function prev_assets(page_list, type)
{
    var result= check_url();

    if(result!="" || result!='undefined' || typeof(result)!='undefined')
    {
        //result= parseInt(result);
        var prev_item =  result-1;
    }
    else
    {
        var prev_item  = 0;
    }

    if (prev_item >= 0)
    {
        pagination_assets(prev_item,page_list, type);
    }


}


function next_assets(page_list, type)
{
    var result= check_url();

    if(result!="" || result!='undefined' || typeof(result)!='undefined')
    {
        result=parseInt(result);
        var next_item =result+1;
        var last_page= $('.last_item').html();
        last_page=parseInt(last_page);

        if(next_item>last_page)
        {
            next_item=last_page;

        }
    }
    else
    {
        var next_item  = 0;
    }
    if (result <= last_page ) {
        pagination_assets(next_item,page_list, type);
    }


}

function pagination_assets(page_id,page_list, type)
{
    var url = window.location.href;

    //console.log(url);
    //console.log(window.location.hash);
    if (page_id)
    {
        //window.location.hash='pagina='+$("#"+page_id).attr('id');
        window.location.hash='pagina='+page_id;

    }
    //get all filter parameters
    var s_public_name = getUrlParameter('s_public_name');
    var s_file_type = getUrlParameter('s_file_type');
    var s_date_start = getUrlParameter('s_date_start');
    var s_date_end = getUrlParameter('s_date_end');

    if(typeof s_public_name == 'undefined')
    {
        s_public_name = '';
    }
    if(typeof s_file_type == 'undefined')
    {
        s_file_type = '';
    }
    if(typeof s_date_start == 'undefined')
    {
        s_date_start = '';
    }
    if(typeof s_date_end == 'undefined')
    {
        s_date_end = '';
    }

    //console.log(window.location.hash);
    // return false;

    var urlPath;
    if (page_list == 'browser')
    {
        urlPath = '/admin-1000/browse.php?type='+type;
    }
    else
    {
        urlPath = '/admin-1000/ajax/show-assets.php';
    }
    $("#loading").show();
    $("#list_results").hide();
    $.ajax({
        type: 'POST',
        url: urlPath,
        data: 'pageId='+ page_id+"&s_file_type="+s_file_type+"&s_public_name="+s_public_name+"&s_date_start="+s_date_start+"&s_date_end="+s_date_end,
        success: function(response)
        {
            $("#list_results").html(response);
            $("#list_results h2").remove();

            //console.log(page_id);
            $(".list_pagination li a").removeClass("curent_item").addClass("pagination_item");
            $(".list_pagination li").removeClass("curent_item");
            $("#"+page_id).removeClass("pagination_item").addClass("curent_item");
            $("#"+page_id+"botom").removeClass("pagination_item").addClass("curent_item");

            $("#loading").hide();
            $("#list_results").show();

        }
    });
}

function edit_asset_item(asset_id, page_no)
{
    $('#editAssetModalLabel').attr('asset_id', asset_id);
    console.log('asset_id '+ asset_id);
    $.ajax({
        url: '/admin-1000/ajax/get_asset_form.php?asset_id='+asset_id+'&page_no='+page_no,
        type: 'GET',
        cache: false,
        dataType:'json',
        success: function(result) {
            $("#editAssetModalBody").html( result.modal_body );
        }
    });
    $('#editAssetModal').modal('show');
}

function show_asset_item(asset_id)
{
    $('#showAssetModalLabel').attr('asset_id', asset_id);
    // console.log('article_id '+ article_id);
    $.ajax({
        url: '/admin-1000/ajax/get_asset_info.php?asset_id='+asset_id,
        type: 'GET',
        cache: false,
        dataType:'json',
        success: function(result) {
            $("#showAssetModalBody").html( result.modal_body );
        }
    });
    $('#showAssetModal').modal('show');
}

function delete_asset_item(asset_id)
{
    if (confirm('Are you sure you want to delete this asset?'))
    {
        $.ajax({
            url: '/admin-1000/ajax/assets-delete.php?asset_id='+asset_id,
            type: 'GET',
            cache: false,
            success: function(result) {
               // window.location = "/admin-1000/?action=admin-assets";
                window.location.reload(true);
            }
        });
    }
    else
    {
        return false;
    }

}
/* END ASSESTS ACTIONS*/


function saveUser(form_id, user_type, action) {

    //make a short validation
    var msgError = '';
    var profile_message_holder = form_id+'_message_div';
    //update only info
    var profile_email = $('#email').val();
    var profile_user_name = $('#user_name').val();
    if (profile_email == '')
    {
        msgError += '<div>Email Address is mandatory.</div>';
    }
    else if (!validateEmail(profile_email))
    {
        msgError += '<div>Invalid Email Address.</div>';
    }
    profile_message_holder = 'form_users_message_div'; //form_users_message_div

    if (profile_user_name == '')
    {
        msgError += '<div>User name is mandatory.</div>';
    }

        var user_type_selected = $('#user_type').val();
        if (user_type_selected == '')
        {
            msgError += '<div>User Type is mandatory.</div>';
        }
        var password = $('#password').val();
        if (password == '')
        {
            msgError += '<div>Password is mandatory.</div>';
        }


    if (msgError == '')
    {

        var m_data = new FormData();
        m_data.append( 'user_name', $('#user_name').val());
        m_data.append( 'email', $('#email').val());
        m_data.append( 'password', $('#password').val());
        m_data.append( 'user_type', $('#user_type').val());
        m_data.append( 'location', $('#location').val());
        m_data.append( 'country', $('#country').val());
        m_data.append( 'user_birth_date_day', $('#user_birth_date_day').val());
        m_data.append( 'user_birth_date_month', $('#user_birth_date_month').val());
        m_data.append( 'user_birth_date_year', $('#user_birth_date_year').val());

        m_data.append( 'myfiles', $('input[name=myfiles]')[0].files[0]);
        $.ajax({
            url: '/admin-1000/ajax/save_user.php',
            type: 'POST',
            data: m_data,
            cache: false,
            dataType:'json',
            processData: false,
            contentType: false,
            success: function(result) {
                if (result.response == 'ok') {
                    if (user_type == 'front_user')
                    {
                        window.location = '/admin-1000/?action=admin-front-users';
                    }
                    else
                    {
                        window.location = '/admin-1000/?action=admin-users';
                    }
                    $("#"+profile_message_holder).html('Profile successfully saved!').removeClass('text-error').addClass("text-success");
                }
                else
                {

                    $("#"+profile_message_holder).html(result.response ).addClass('text-error').removeClass("text-success");
                }
            },
            error: function(result) {
                console.log(result);
                return false;
            }
        });
    }
    else
    {
        //alert(msgError);
        $("#"+profile_message_holder).html(msgError);
        return false;
    }
}


function edit_user()
{
    //make a short validation
    var msgError = '';
    var profile_message_holder = 'error_message_edit_user';
    //update only info
    var profile_email = $('#edit_email').val();
    var profile_user_name = $('#edit_user_name').val();
    if (profile_email == '')
    {
        msgError += 'Email Address is mandatory';
    }
    if (!validateEmail(profile_email))
    {
        msgError += 'Invalid Email Address';
    }
    profile_message_holder = 'details_message_response';

    if (profile_user_name == '')
    {
        msgError += 'User name is mandatory';
    }


    if (msgError == '')
    {
        var page_no = $('#page_no').val();
        var user_type_section = $('#user_type_section').val();



        var m_data = new FormData();
        m_data.append( 'page_no', $('#page_no').val());
        m_data.append( 'user_id', $('#edit_user_id').val());
        m_data.append( 'user_name', $('#edit_user_name').val());
        m_data.append( 'email', $('#edit_email').val());
        m_data.append( 'password', $('#edit_password').val());
        m_data.append( 'user_type', $('#edit_user_type').val());
        m_data.append( 'location', $('#edit_location').val());
        m_data.append( 'country', $('#edit_country').val());
        m_data.append( 'user_birth_date_day', $('#edit_user_birth_date_day').val());
        m_data.append( 'user_birth_date_month', $('#edit_user_birth_date_month').val());
        m_data.append( 'user_birth_date_year', $('#edit_user_birth_date_year').val());

        m_data.append( 'myfiles', $('input[name=edit_myfiles]')[0].files[0]);

        $.ajax({
            url: '/admin-1000/ajax/edit_user.php',
            type: 'POST',
            data: m_data,
            cache: false,
            dataType:'json',
            processData: false,
            contentType: false,
            success: function(result) {
               // console.log('page_no '+ result.response);
                if (result.response == 'ok') {

                    $('#editUserModal').modal('hide');
                    if (result.user_type_section == 'front_user')
                    {
                        window.location = '/admin-1000/?action=admin-front-users';
                    }
                    else
                    {
                        window.location = '/admin-1000/?action=admin-users';
                    }
                    window.location = "/admin-1000/?action=admin-articles#pagina="+page_no;
                    window.location.reload(true);
                }
                else
                {

                    $("#editUserModalBody #error_messaj_edit_article").html(result.response );
                }
            }
        });
    }
    else
    {
        alert(msgError);
        return false;
    }
}
function edit_user_item(user_id, page_no, user_type)
{
    $('#editUserModalLabel').attr('user_id', user_id);
    //console.log('user_id '+ user_id);
    $.ajax({
        url: '/admin-1000/ajax/get_user_form.php?user_id='+user_id+'&page_no='+page_no+'&user_type_section='+user_type,
        type: 'GET',
        cache: false,
        dataType:'json',
        success: function(result) {
            $("#editUserModalBody").html( result.modal_body );
        }
    });
    $('#editUserModal').modal('show');
}

function validateEmail(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}

function toggle_menu_table(menu_level){
    $('#level_'+menu_level+'_menu').toggle();
    $('#level_'+menu_level+'_menu_arrow').toggleClass('glyphicon-chevron-down glyphicon-chevron-up');

}


function openBrowseServer(input_file_url, event)
{
    event.preventDefault();
    var urlPath;
    urlPath = '/admin-1000/browse.php?type=images&source=non_ckeditor&input_file_url='+input_file_url;

    var w = window.open(urlPath, "popupWindow", "width=900, height=900, scrollbars=yes");
    var $w = $(w.document.body);

}


//======== START Categories ========

function category_item(category_id, action_type)
{

    $.ajax({
        url: '/admin-1000/ajax/get_category_form.php?category_id='+category_id+'&action_type='+action_type,
        type: 'GET',
        cache: false,
        dataType:'json',
        success: function(result) {
            if (action_type == 'add')
            {
                $("#categoryModalLabel").html('Add category');
            }
            else
            {
                $("#categoryModalLabel").html('Edit category');
            }
            $("#categoryModal #frm_modal_btn").removeAttr("onclick");
            $("#categoryModal #frm_modal_btn").off("click");
            $("#categoryModal #frm_modal_btn").on("click", function () {
                save_category('frm_category');
            });
            $("#categoryModalBody").html( result.modal_body );
        }
    });
    $('#categoryModal').modal('show');
}

function save_category(form_id)
{

    $.ajax({
        url: '/admin-1000/ajax/save_category.php',
        type: 'POST',
        data: $('#'+form_id).serialize(),
        cache: false,
        dataType:'json',
        success: function(result) {
            if (result.response == 'ok') {
                window.location.reload(true);
            }
            else
            {
                $("#categoryModalBody #error_message").html(result.response );
            }
        }
    });
}

function delete_category_item(category_id)
{
    if (confirm('Are you sure you want to delete this category?'))
    {
        $.ajax({
            url: '/admin-1000/ajax/delete-category.php?category_id='+category_id,
            type: 'GET',
            cache: false,
            dataType:'json',
            success: function(result) {
                if (result.response == "ok")
                {
                    window.location.reload(true);
                }
            }
        });
    }
    else
    {
        return false;
    }
}
//======== END Categories ========


//======== START Comments ========

function current_comments(comment_type)
{
    var result= check_url();

    if(result!="" || result!='undefined' || typeof(result)!='undefined')
    {
        //result= parseInt(result);
        var prev_item =  result;
    }
    else
    {
        var prev_item  = 1;
    }

    if (prev_item > 0)
    {
        pagination_comments(prev_item, comment_type);
    }


}

function pagination_comments(page_id,comment_type)
{
    var url = window.location.href;

    //console.log(url);
    //console.log(window.location.hash);
    if (page_id)
    {
        //window.location.hash='pagina='+$("#"+page_id).attr('id');
        window.location.hash='pagina='+page_id;

    }
    //get all filter parameters
    var s_title = getUrlParameter('s_title');
    var s_date_start = getUrlParameter('s_date_start');
    var s_date_end = getUrlParameter('s_date_end');
    var s_menu_item_level_1 = getUrlParameter('s_menu_item_level_1');
    var s_menu_item_level_2 = getUrlParameter('s_menu_item_level_2');
    var s_menu_item_level_3 = getUrlParameter('s_menu_item_level_3');


    //console.log(window.location.hash);
    // return false;
    $("#loading").show();
    $("#list_results").hide();
    $.ajax({
        type: 'POST',
        url: '/admin-1000/ajax/show-comments.php',
        data: 'pageId='+ page_id+"&s_title="+s_title+"&s_date_start="+s_date_start+"&s_date_end="+s_date_end+"&s_menu_item_level_1="+s_menu_item_level_1+"&s_menu_item_level_2="+s_menu_item_level_2+"&s_menu_item_level_3="+s_menu_item_level_3+"&comment_type="+comment_type,
        success: function(response)
        {
            $("#list_results").html(response);
            //console.log(page_id);
            $(".list_pagination li a").removeClass("curent_item").addClass("pagination_item");
            $(".list_pagination li").removeClass("curent_item");
            $("#"+page_id).removeClass("pagination_item").addClass("curent_item");
            $("#"+page_id+"botom").removeClass("pagination_item").addClass("curent_item");

            $("#loading").hide();
            $("#list_results").show();
        }
    });
}


function prev_comments(comment_type)
{
    var result= check_url();

    if(result!="" || result!='undefined' || typeof(result)!='undefined')
    {
        //result= parseInt(result);
        var prev_item =  result-1;
    }
    else
    {
        var prev_item  = 0;
    }

    if (prev_item >= 0)
    {
        pagination_comments(prev_item, comment_type);
    }


}


function next_comments(comment_type)
{
    var result= check_url();

    if(result!="" || result!='undefined' || typeof(result)!='undefined')
    {
        result=parseInt(result);
        var next_item =result+1;
        var last_page= $('.last_item').html();
        last_page=parseInt(last_page);

        if(next_item>last_page)
        {
            next_item=last_page;

        }
    }
    else
    {
        var next_item  = 0;
    }
    if (result <= last_page ) {
        pagination_comments(next_item, comment_type);
    }


}


function delete_comment_item(comment_id, replies_no)
{


    replies_msg  = 'Sunteti sigur ca doriti sa continuati?';

    if (confirm(replies_msg))
    {
        $.ajax({
            url: '/admin-1000/ajax/delete-comment.php?id='+comment_id,
            type: 'GET',
            cache: false,
            dataType:'json',
            success: function(result) {
                if (result.response == "ok")
                {
                    window.location.reload(true);
                }
            }
        });
    }
    else
    {
        return false;
    }

}

function edit_comment()
{
    var page_no = $('#page_no').val();


    $.ajax({
        url: '/admin-1000/ajax/edit_comment.php',
        type: 'POST',
        data: $("#frm_edit_comment").serialize(),
        cache: false,
        dataType:'json',
        success: function(result) {
            console.log('page_no '+ result.response);
            if (result.response == 'ok') {
                $('#editCommentModal').modal('hide');
                // window.location = "/admin-1000/?action=admin-articles#pagina="+page_no;
                window.location.reload(true);
            }
            else
            {

                $("#editCommentModalBody #error_messaj_edit_comment").html(result.response );
            }
        }
    });
}
function edit_comment_item(comment_id, page_no)
{
    $('#editCommentModalLabel').attr('comment_id', comment_id);
    //console.log('comment_id '+ comment_id);
    $.ajax({
        url: '/admin-1000/ajax/get_comment_form.php?id='+comment_id+'&page_no='+page_no,
        type: 'GET',
        cache: false,
        dataType:'json',
        success: function(result) {
            $("#editCommentModalBody").html( result.modal_body );
        }
    });
    $('#editCommentModal').modal('show');
}

function approve_reject_comment(comment_id, moderation)
{
    var page_no = $('#page_no').val();

    $.ajax({
        url: '/admin-1000/ajax/approve_reject_comment.php',
        type: 'GET',
        data: {id: comment_id, mode:moderation },
        cache: false,
        dataType:'json',
        success: function(result) {
           // console.log('page_no '+ result.response);
            if (result.response == 'ok') {
                window.location.reload(true);
            }
            else
            {

                $("#error_messaj_action_comment").html(result.response );
            }
        }
    });
}

//======== END Comments ========


//======== START EMAILS MANAGEMENT ========

/*****  PAGINATION ***/

function pagination_emails(page_id)
{
    var url = window.location.href;

    //console.log(url);
    //console.log(window.location.hash);
    if (page_id)
    {
        //window.location.hash='pagina='+$("#"+page_id).attr('id');
        if (page_id > 1 ) {
            window.location.hash='pagina='+page_id;
        }

    }

    //console.log(window.location.hash);
    // return false;
    $("#loading").show();
    $("#list_results").hide();
    $.ajax({
        type: 'POST',
        url: '/admin-1000/ajax/show-emails-management.php',
        data: 'pageId='+ page_id, // admin or regular_user
        success: function(response)
        {
            $("#list_results").html(response);
            //console.log(page_id);
            $(".list_pagination li a").removeClass("curent_item").addClass("pagination_item");
            $(".list_pagination li").removeClass("curent_item");
            $("#"+page_id).removeClass("pagination_item").addClass("curent_item");
            $("#"+page_id+"botom").removeClass("pagination_item").addClass("curent_item");

            $("#loading").hide();
            $("#list_results").show();
        }
    });
}


function prev_emails()
{
    var result= check_url();

    if(result!="" || result!='undefined' || typeof(result)!='undefined')
    {
        //result= parseInt(result);
        var prev_item =  result-1;
    }
    else
    {
        var prev_item  = 0;
    }

    if (prev_item >= 0)
    {
        var url = window.location.href;
        var sort = url.match("type=[a-z]+");

        pagination_emails(prev_item);
    }


}


function next_emails()
{
    var result= check_url();

    if(result!="" || result!='undefined' || typeof(result)!='undefined')
    {
        result=parseInt(result);
        var next_item =result+1;
        var last_page= $('.last_item').html();
        last_page=parseInt(last_page);

        if(next_item>last_page)
        {
            next_item=last_page;

        }
    }
    else
    {
        var next_item  = 0;
    }
    if (result <= last_page ) {
        var url = window.location.href;
        var sort = url.match("type=[a-z]+");


        pagination_emails(next_item);
    }


}


function current_emails()
{
    var result= check_url();

    if(result!="" || result!='undefined' || typeof(result)!='undefined')
    {
        //result= parseInt(result);
        var prev_item =  result;
    }
    else
    {
        var prev_item  = 1;
    }

    if (prev_item > 0)
    {
        pagination_emails(prev_item);
    }


}

function add_emai_managementl()
{
    $.ajax({
        url: '/admin-1000/ajax/get_email_form.php',
        type: 'GET',
        cache: false,
        dataType:'json',
        success: function(result) {
            $("#emailModalLabel").html( 'Add Email Management' );
            $("#emailModalBody").html( result.modal_body );
        }
    });
    $('#emailModal').modal('show');
}
function save_email_management()
{
    var  formElem = $("#frm_email_save");

    $.ajax({
        url: '/admin-1000/ajax/save_email.php',
        type: 'POST',
        data: formElem.serialize(),
        cache: false,
        dataType:'json',
        success: function(result) {
            //  console.log('page_no '+ result.response);
            if (result.response == 'ok') {
                $('#newModal').modal('hide');
                window.location.reload(true);
            }
            else
            {
                $("#emailModal #error_message_save_email").html(result.response );
            }
        }
    });
}
function edit_email_management(email_management_id, page_no)
{

    // event.stopPropagation();
    //  event.preventDefault();

    $.ajax({
        url: '/admin-1000/ajax/get_email_form.php?email_management_id='+email_management_id+'&page_no='+page_no,
        type: 'GET',
        cache: false,
        dataType:'json',
        success: function(result) {
            $("#emailModalLabel").html( 'Edit Email Management' );
            $("#emailModalBody").html( result.modal_body );
        }
    });
    $('#emailModal').modal('show');
}

function update_email_management_status(email_management_id, event)
{

    event.stopPropagation();
    event.preventDefault();

    var current_element = $('#update-status-'+email_management_id);
    var current_status = current_element.attr('current-status');
    var status, error_class, icon_class, old_error_class, old_icon_class, icon_status_class, old_icon_status_class;

    var icon_status_element = $('#icon-status-'+email_management_id);

    if (current_status == 1) {
        status = 0;
        error_class = 'text-error';
        icon_class = 'glyphicon-ok';

        old_error_class = 'text-success';
        old_icon_class = 'glyphicon-ok';

        old_icon_status_class = '';
        icon_status_class = 'glyphicon-alert';
    }
    else
    {
        status = 1;
        error_class = 'text-success';
        icon_class = 'glyphicon-ok';

        old_error_class = 'text-error';
        old_icon_class = 'glyphicon-ok';

        old_icon_status_class = 'glyphicon-alert';
        icon_status_class = '';

    }

    $.ajax({
        url: '/admin-1000/ajax/update-email-management-status.php?email_management_id='+email_management_id+'&action=update&status='+status,
        type: 'GET',
        cache: false,
        dataType:'json',
        success: function(result) {
            if (result.response == 'ok') {
                current_element.attr('current-status', status);
                current_element.removeClass(old_error_class).addClass(error_class);
                current_element.find('span').removeClass(old_icon_class).addClass(icon_class);

                icon_status_element.removeClass(old_icon_status_class).addClass(icon_status_class);
            }
        }
    });


}

function remove_email_management(email_management_id, event)
{

    event.stopPropagation();
    event.preventDefault();
    if (confirm('Are you sure you want to delete this email?'))
    {
        $.ajax({
            url: '/admin-1000/ajax/delete_email.php?action=delete&email_management_id='+email_management_id,
            type: 'GET',
            cache: false,
            success: function(result) {
                window.location.reload(true);
            }
        });
    }
    else
    {
        return false;
    }

}

//======== END EMAILS MANAGEMENT ========





//======== START SETTINGS ========

function setting_item(setting_id, action_type)
{

    $.ajax({
        url: '/admin-1000/ajax/get_setting_form.php?setting_id='+setting_id+'&action_type='+action_type,
        type: 'GET',
        cache: false,
        dataType:'json',
        success: function(result) {
            if (action_type == 'add')
            {
                $("#settingModalLabel").html('Add setting');
            }
            else
            {
                $("#settingModalLabel").html('Edit setting');
            }
            $("#settingModal #frm_modal_btn").removeAttr("onclick");
            $("#settingModal #frm_modal_btn").off("click");
            $("#settingModal #frm_modal_btn").on("click", function () {
                save_setting('frm_setting');
            });
            $("#settingModalBody").html( result.modal_body );
        }
    });
    $('#settingModal').modal('show');
}

function save_setting(form_id)
{

    $.ajax({
        url: '/admin-1000/ajax/save_setting.php',
        type: 'POST',
        data: $('#'+form_id).serialize(),
        cache: false,
        dataType:'json',
        success: function(result) {
            if (result.response == 'ok') {
                window.location.reload(true);
            }
            else
            {
                $("#settingModalBody #error_message").html(result.response );
            }
        }
    });
}

function delete_setting_item(setting_id)
{
    if (confirm('Are you sure you want to delete this setting?'))
    {
        $.ajax({
            url: '/admin-1000/ajax/delete-setting.php?setting_id='+setting_id,
            type: 'GET',
            cache: false,
            dataType:'json',
            success: function(result) {
                if (result.response == "ok")
                {
                    window.location.reload(true);
                }
            }
        });
    }
    else
    {
        return false;
    }
}
//======== END SETTINGS ========
