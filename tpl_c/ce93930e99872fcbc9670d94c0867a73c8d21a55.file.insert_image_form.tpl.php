<?php /* Smarty version Smarty-3.1.21-dev, created on 2016-04-04 15:41:48
         compiled from "/var/www/imageupload-rares//tpl/insert_image_form.tpl" */ ?>
<?php /*%%SmartyHeaderCode:20666401625702610c37cb70-12544966%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'ce93930e99872fcbc9670d94c0867a73c8d21a55' => 
    array (
      0 => '/var/www/imageupload-rares//tpl/insert_image_form.tpl',
      1 => 1459327076,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '20666401625702610c37cb70-12544966',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'file_name' => 0,
    'description' => 0,
    'sitekey' => 0,
    'errors' => 0,
    'err' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5702610c3831d1_63494310',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5702610c3831d1_63494310')) {function content_5702610c3831d1_63494310($_smarty_tpl) {?><div class="image-form-wrapper">

  <form action="" method="post" id="imgupload" name="imgupload" enctype="multipart/form-data">

    <div class="form-group">

      <label for="file">Upload Image here</label>

      <input form="imgupload" class="form-control" type="file" id="file" name="file" required  />

    </div>

    <div class="form-group">

      <label for="file_name">File name</label>

      <input form="imgupload" class="form-control" type="text" id="file_name" name="file_name" required value="<?php echo $_smarty_tpl->tpl_vars['file_name']->value;?>
" placeholder="Ex: my_foto(.jpg)" />

    </div>

    <div class="form-group">

      <label for="description">Description</label>

      <textarea form="imgupload" class="form-control" type="text" id="description" name="description" required  ><?php echo $_smarty_tpl->tpl_vars['description']->value;?>
</textarea>

    </div>

    <div class="form-group">

      <div class="g-recaptcha" data-sitekey="<?php echo $_smarty_tpl->tpl_vars['sitekey']->value;?>
" id="recaptha_id_4comment" form="imgupload" style="width:100%"></div>

    </div>

    <div class="form-group">

      <input type="hidden" name="action" value="add_image" />

      <input form="imgupload" class="form-control" type="submit" id="description"  required value="Submit photo" />

    </div>

  </form>

</div>

<div class="error-section">
  <ul>
  <?php  $_smarty_tpl->tpl_vars['err'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['err']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['errors']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['err']->key => $_smarty_tpl->tpl_vars['err']->value) {
$_smarty_tpl->tpl_vars['err']->_loop = true;
?>
      <li><?php echo $_smarty_tpl->tpl_vars['err']->value;?>
</li>
  <?php } ?>
  </ul>
</div>
<?php }} ?>
