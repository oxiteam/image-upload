<?php /* Smarty version Smarty-3.1.21-dev, created on 2016-04-04 17:47:58
         compiled from "/var/www/imageupload-rares/tpl/checkout_payment.tpl" */ ?>
<?php /*%%SmartyHeaderCode:62999375257027e9e2942c4-63598001%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'b18d46b5359cd1a77953ee7e26ec9cdd18c48f3c' => 
    array (
      0 => '/var/www/imageupload-rares/tpl/checkout_payment.tpl',
      1 => 1459777725,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '62999375257027e9e2942c4-63598001',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_57027e9e29aec2_40502703',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_57027e9e29aec2_40502703')) {function content_57027e9e29aec2_40502703($_smarty_tpl) {?><div class="wrapper">
<div id="divPageOuter" class="PageOuter">
    <div id="divPage" class="Page">

<!--  Remove in production
      <div id="divPreviewMode">Payment Form is in Preview Mode. Real transactions will not be processed.</div>
      <div id="divPreviewMode2">Click <span style="font-weight:bold;">Pay Now</span> to preview the Receipt Page. You must fill in with Test Card Numbers from <a target="_BLANK" href="http://developer.authorize.net/hello_world/testing_guide/">Authorize.net Testing Guide</a></div>
-->
<form id="formPayment" autocomplete="off" method="post" action="">

<input type="HIDDEN" name="action" id="action" value="pay_now">
* Required Fields

<hr class="HrLineItem">

        <div id="divOrderDetailsBottom">

        </div>
        <div id="divOrderDetailsBottomSpacer"></div>

                <table class="SectionHeadingBorder" id="tablePaymentMethodHeading" role="presentation">
          <tbody><tr>
            <td><h2 class="Label">Payment Information</h2></td>
          </tr>
        </tbody></table>

        <div id="divPaymentMethod">
<fieldset>
<table id="tablePaymentMethod" role="presentation">
  <tbody><tr>
    <td class="LabelColPayBy">Pay by&nbsp;</td>
      <td class="DataColPayBy"><input id="pmCC" type="radio" class="input_radio" checked="" name="x_method" value="cc" onclick="paymentMethod_onClick()"></td>
      <td class="DataColPayBy"><label for="pmCC"><img src="content/creditcard.png" alt="Credit/Debit Card"></label></td>

  </tr>
</tbody></table>
</fieldset><input type="hidden" name="x_method_available" value="true">
</div>

<div id="divCreditCardInformation">
<table id="tableCreditCardInformation" role="presentation">
  <tbody>
  <tr>
    <td class="SpacerRow2" colspan="2">&nbsp;</td>
  </tr>
  <tr id="trCCInfoBold" style="display: none;">
    <td class="LabelColCC">&nbsp;</td>
    <td class="DataColCC" style="font-weight: bold;">Credit Card Information</td>
  </tr>
  <tr id="trAcceptedCardImgs">
    <td class="LabelColCC">&nbsp;</td>
    <td class="DataColCC"><img src="content/V.gif" width="43" height="26" title="Visa" alt="Visa"><img src="content/MC.gif" width="41" height="26" title="MasterCard" alt="MasterCard"><img src="content/Amex.gif" width="40" height="26" title="American Express" alt="American Express"><img src="content/Disc.gif" width="40" height="26" title="Discover" alt="Discover"><img src="content/JCB.gif" width="21" height="26" title="JCB" alt="JCB"></td>
  </tr>
  <tr>
    <td class="LabelColCC"><label for="x_card_num"><span class="Hidden">(enter number without spaces or dashes)</span>Card Number</label>:&nbsp;</td>
    <td class="DataColCC"><input type="text" class="input_text" id="x_card_num" name="x_card_num" maxlength="16" autocomplete="off" value="0000000000000000" aria-required="true" aria-invalid="false">*&nbsp;<span class="Comment">(enter number without spaces or dashes)</span></td>
  </tr>
  <tr>
    <td class="LabelColCC"><label for="x_exp_date"><span class="Hidden">(mmyy)</span>Expiration Date</label>:&nbsp;</td>
    <td class="DataColCC"><input type="text" class="input_text" id="x_exp_date" name="x_exp_date" maxlength="4" autocomplete="off" value="" aria-required="true" aria-invalid="false">*&nbsp;<span class="Comment">(mmyy)</span></td>
  </tr>
  <tr>
    <td class="LabelColCC"><label for="x_exp_date"><span class="Hidden"><a href="https://www.cvvnumber.com/cvv.html" target="_blank" style="font-size:11px">What is my CVV code?</a> </span>CCV</label>:&nbsp;</td>
    <td class="DataColCC"><input type="text" class="input_text" id="x_ccv_code" name="x_ccv_code" maxlength="4" autocomplete="off" value="" aria-required="true" aria-invalid="false">*&nbsp;<span class="Comment">(enter number without spaces or dashes)</span></td>
  </tr>
</tbody></table>
</div>

        <div id="divBankAccountInformation" style="display: none;">
<table id="tableBankAccountInformation" role="presentation">
  <tbody><tr>
    <td class="SpacerRow2" colspan="2">&nbsp;</td>
  </tr>
  <tr id="trBankAcctInfoBold" style="display: none;">
    <td class="LabelColBank">&nbsp;</td>
    <td class="DataColBank" style="font-weight: bold;">Bank Account Information</td>
  </tr>
  <tr>
  <td class="LabelColBank"><label for="x_bank_name"><span class="Hidden"></span>Bank Name</label>:&nbsp;</td>
  <td class="DataColBank"><input type="text" class="input_text" id="x_bank_name" name="x_bank_name" maxlength="20" autocomplete="off" value="" aria-required="false" aria-invalid="false">&nbsp;<span class="Comment"></span></td>
</tr>
  <tr>
  <td class="LabelColBank"><label for="x_bank_acct_num"><span class="Hidden">(enter number without spaces or dashes)</span>Bank Account Number</label>:&nbsp;</td>
  <td class="DataColBank"><input type="text" class="input_text" id="x_bank_acct_num" name="x_bank_acct_num" maxlength="20" autocomplete="off" value="" aria-required="true" aria-invalid="false">*&nbsp;<span class="Comment">(enter number without spaces or dashes)</span></td>
</tr>
  <tr>
  <td class="LabelColBank"><label for="x_bank_aba_code"><span class="Hidden"><a id="aAbaWhatsThis" href="https://account.authorize.net/help/Miscellaneous/Pop-up_Terms/ALL/ABA_Routing_Number.htm" target="_blank" onclick="javascript:return PopupLink(this);">What's this?</a></span>ABA Routing Number</label>:&nbsp;</td>
  <td class="DataColBank"><input type="text" class="input_text" id="x_bank_aba_code" name="x_bank_aba_code" maxlength="9" autocomplete="off" value="" aria-required="true" aria-invalid="false">*&nbsp;<span class="Comment"><a id="aAbaWhatsThis" href="https://account.authorize.net/help/Miscellaneous/Pop-up_Terms/ALL/ABA_Routing_Number.htm" target="_blank" onclick="javascript:return PopupLink(this);">What's this?</a></span></td>
</tr>
  <tr>
  <td class="LabelColBank"><label for="x_bank_acct_name"><span class="Hidden"></span>Name On Account</label>:&nbsp;</td>
  <td class="DataColBank"><input type="text" class="input_text" id="x_bank_acct_name" name="x_bank_acct_name" maxlength="22" autocomplete="off" value="" aria-required="false" aria-invalid="false">&nbsp;<span class="Comment"></span></td>
</tr>
  <tr>
  <td class="LabelColBank"><label for="x_bank_acct_type"><span class="Hidden"></span>Bank Account Type</label>:&nbsp;</td>
  <td class="DataColBank"><select id="x_bank_acct_type" name="x_bank_acct_type" size="1">
<option value="CK" selected="">Personal Checking</option>
<option value="SA">Personal Savings</option>
<option value="BC">Business Checking</option>

</select>&nbsp;<span class="Comment"></span></td>
</tr>

</tbody></table>
</div>

<div class="errors">

</div>

        <?php echo '<script'; ?>
 type="text/javascript" language="javascript">
          var oTrChkCopyBill = document.getElementById('trChkCopyBill');
          if (null != oTrChkCopyBill) oTrChkCopyBill.style.display = '';
        <?php echo '</script'; ?>
>




        <div id="divSubmit">
        <hr id="hrButtonsBefore">
        <table id="tableButtons" role="presentation">
          <tbody><tr>
            <td class="SpacerRow1" colspan="3">&nbsp;</td>
          </tr>
          <tr>
            <td id="tdVerifiedMerchant">&nbsp;</td>
            <td id="tdButtons">
              <table id="tableButtonsInner" role="presentation">
                <tbody><tr>
                  <td id="tdSubmit">
                    <input type="submit" id="btnSubmit" value="   Pay Now   " onclick="return onSubmit();">
                  </td>
                  <td id="tdCancelOrder" style="display:none;">
                    <input type="button" id="btnCancelOrder" value="Cancel Order" onclick="return cancelOrder_onClick();">
                  </td>
                  <td>

                  </td>
                </tr>
              </tbody></table>
            </td>
            <td id="tdButtonsAfter">&nbsp;</td>
          </tr>
        </tbody></table>
        </div>
      </form>

    </div> <!-- entire BODY -->
  </div>
</div>
  <div class="PageAfter"></div>
        <?php echo '<script'; ?>
 type="text/javascript" language="javascript">
          paymentMethod_onClick();
          var oTrCCInfo = document.getElementById('trCCInfoBold');
          var oTrBankAcctInfo = document.getElementById('trBankAcctInfoBold');
          if (null != oTrCCInfo) oTrCCInfo.style.display = 'none';
          if (null != oTrBankAcctInfo) oTrBankAcctInfo.style.display = 'none';
        <?php echo '</script'; ?>
>
<?php }} ?>
