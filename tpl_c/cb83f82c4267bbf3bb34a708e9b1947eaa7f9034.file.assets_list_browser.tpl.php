<?php /* Smarty version Smarty-3.1.21-dev, created on 2016-03-31 12:53:03
         compiled from "/var/www/imageupload-rares/tpl/admin-1000//assets_list_browser.tpl" */ ?>
<?php /*%%SmartyHeaderCode:64200166756fcf37f65c7a9-26228726%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'cb83f82c4267bbf3bb34a708e9b1947eaa7f9034' => 
    array (
      0 => '/var/www/imageupload-rares/tpl/admin-1000//assets_list_browser.tpl',
      1 => 1446565255,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '64200166756fcf37f65c7a9-26228726',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'root_url' => 0,
    'action' => 0,
    'CKEditorFuncNum' => 0,
    'pageTitle' => 0,
    'current_thread' => 0,
    'IMAGES_PATH_DIR' => 0,
    'no_of_page' => 0,
    'link_pagination' => 0,
    'link_ajax' => 0,
    'assets_type' => 0,
    'page_no' => 0,
    'start' => 0,
    'min_page' => 0,
    'max_page' => 0,
    'allAssets' => 0,
    'assets' => 0,
    'ASSETS_PATH_DIR' => 0,
    'source' => 0,
    'input_file_url' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_56fcf37f6bd901_32063884',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_56fcf37f6bd901_32063884')) {function content_56fcf37f6bd901_32063884($_smarty_tpl) {?><!doctype html>
<html>
<head>
    <title>Admin CMS MS</title>

    <meta name="viewport" content="width=1024" />
    <meta charset="utf-8" />

    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet" >

    <link href="<?php echo $_smarty_tpl->tpl_vars['root_url']->value;?>
/css/style.css" rel="stylesheet" type="text/css" />

    <link href="<?php echo $_smarty_tpl->tpl_vars['root_url']->value;?>
/admin-1000/css/style_admin.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo $_smarty_tpl->tpl_vars['root_url']->value;?>
/admin-1000/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />


    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/themes/smoothness/jquery-ui.css">

    <?php echo '<script'; ?>
 src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['root_url']->value;?>
/admin-1000/js/bootstrap-datepicker.min.js"><?php echo '</script'; ?>
>

    <?php if ($_smarty_tpl->tpl_vars['action']->value=='admin-assets') {?>
        <?php echo '<script'; ?>
 src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/jquery-ui.min.js"><?php echo '</script'; ?>
>
    <?php }?>



    <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['root_url']->value;?>
/admin-1000/js/main_admin.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['root_url']->value;?>
/ckeditor/ckeditor.js"><?php echo '</script'; ?>
>

</head>
<body>
<div class="wraper">
    <div class="content-wrap page">

        <div class="content">
            <input type="hidden" name="CKEditorFuncNum" id="CKEditorFuncNum" value="<?php echo $_smarty_tpl->tpl_vars['CKEditorFuncNum']->value;?>
">
            <h2 class="tell_pb left"><?php echo $_smarty_tpl->tpl_vars['pageTitle']->value;?>
</h2>
            <div id="loading" <?php if ($_smarty_tpl->tpl_vars['current_thread']->value==1) {?>style="text-align: center; display:block; margin:25px 0;"<?php } else { ?>style="text-align: center; display:none; margin:25px 0;"<?php }?>><img src="<?php echo $_smarty_tpl->tpl_vars['IMAGES_PATH_DIR']->value;?>
/loading.gif"></div>
            <div class="clear">&nbsp;</div>
            <div id="list_results">
                <?php if ($_smarty_tpl->tpl_vars['no_of_page']->value>0) {?>
                    <div class="last_item" style="display: none;"><?php echo $_smarty_tpl->tpl_vars['no_of_page']->value;?>
</div>
                    <div class="paginare">
                        <ul class="list_pagination">
                            <li>
                                <a  href="<?php echo $_smarty_tpl->tpl_vars['link_pagination']->value;?>
#<?php echo $_smarty_tpl->tpl_vars['link_ajax']->value;?>
pagina=1" class="to_left_left" onclick="pagination_assets('1', 'browser', '<?php echo $_smarty_tpl->tpl_vars['assets_type']->value;?>
');return false;"><span aria-hidden="true" class="glyphicon glyphicon-step-backward"></span></a>
                            </li>
                            <li><a  href="<?php echo $_smarty_tpl->tpl_vars['link_pagination']->value;?>
#<?php echo $_smarty_tpl->tpl_vars['link_ajax']->value;?>
pagina=<?php echo $_smarty_tpl->tpl_vars['page_no']->value-1;?>
" class="to_left" onclick="javascript:prev_assets('browser', '<?php echo $_smarty_tpl->tpl_vars['assets_type']->value;?>
');return false;"><span aria-hidden="true" class="glyphicon glyphicon-chevron-left"></span></a></li>
                            <?php $_smarty_tpl->tpl_vars['start'] = new Smarty_Variable;$_smarty_tpl->tpl_vars['start']->step = 1;$_smarty_tpl->tpl_vars['start']->total = (int) ceil(($_smarty_tpl->tpl_vars['start']->step > 0 ? $_smarty_tpl->tpl_vars['no_of_page']->value+1 - (1) : 1-($_smarty_tpl->tpl_vars['no_of_page']->value)+1)/abs($_smarty_tpl->tpl_vars['start']->step));
if ($_smarty_tpl->tpl_vars['start']->total > 0) {
for ($_smarty_tpl->tpl_vars['start']->value = 1, $_smarty_tpl->tpl_vars['start']->iteration = 1;$_smarty_tpl->tpl_vars['start']->iteration <= $_smarty_tpl->tpl_vars['start']->total;$_smarty_tpl->tpl_vars['start']->value += $_smarty_tpl->tpl_vars['start']->step, $_smarty_tpl->tpl_vars['start']->iteration++) {
$_smarty_tpl->tpl_vars['start']->first = $_smarty_tpl->tpl_vars['start']->iteration == 1;$_smarty_tpl->tpl_vars['start']->last = $_smarty_tpl->tpl_vars['start']->iteration == $_smarty_tpl->tpl_vars['start']->total;?>
                                <?php if ($_smarty_tpl->tpl_vars['start']->value>=$_smarty_tpl->tpl_vars['min_page']->value&&$_smarty_tpl->tpl_vars['start']->value<=$_smarty_tpl->tpl_vars['max_page']->value) {?>
                                    <li <?php if ($_smarty_tpl->tpl_vars['start']->value==$_smarty_tpl->tpl_vars['page_no']->value) {?> class="curent_item"<?php }?>>
                                        <a   href="<?php echo $_smarty_tpl->tpl_vars['link_pagination']->value;?>
#<?php echo $_smarty_tpl->tpl_vars['link_ajax']->value;?>
pagina=<?php echo $_smarty_tpl->tpl_vars['start']->value;?>
" class="pagination_item <?php if ($_smarty_tpl->tpl_vars['start']->value==$_smarty_tpl->tpl_vars['page_no']->value) {?> curent_item<?php }?>" id="<?php echo $_smarty_tpl->tpl_vars['start']->value;?>
" onclick="pagination_assets('<?php echo $_smarty_tpl->tpl_vars['start']->value;?>
', 'browser', '<?php echo $_smarty_tpl->tpl_vars['assets_type']->value;?>
');return false;"><?php echo $_smarty_tpl->tpl_vars['start']->value;?>
</a>
                                    </li>
                                <?php }?>
                            <?php }} ?>
                            <li>
                                <a  href="<?php echo $_smarty_tpl->tpl_vars['link_pagination']->value;?>
#<?php echo $_smarty_tpl->tpl_vars['link_ajax']->value;?>
pagina=<?php echo $_smarty_tpl->tpl_vars['page_no']->value+1;?>
" class="to_right" onclick="javascript:next_assets('browser', '<?php echo $_smarty_tpl->tpl_vars['assets_type']->value;?>
');return false;"><span aria-hidden="true" class="glyphicon glyphicon-chevron-right"></span></a></li>
                            <li><a  href="<?php echo $_smarty_tpl->tpl_vars['link_pagination']->value;?>
#<?php echo $_smarty_tpl->tpl_vars['link_ajax']->value;?>
pagina=<?php echo $_smarty_tpl->tpl_vars['no_of_page']->value;?>
" class="to_right_right" onclick="pagination_assets('<?php echo $_smarty_tpl->tpl_vars['no_of_page']->value;?>
', 'browser', '<?php echo $_smarty_tpl->tpl_vars['assets_type']->value;?>
');return false;"><span aria-hidden="true" class="glyphicon glyphicon-step-forward"></span></a></li>
                        </ul>
                    </div>
                <?php }?>
                <div class="clear">&nbsp;</div>


                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>Asset id</th>
                            <th>Download</th>
                            <th>Public name</th>
                            <th>Type</th>
                            <th>Size</th>
                            <th>Dimensions</th>
                            <th>Description</th>
                            <th>Created Date</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php  $_smarty_tpl->tpl_vars['assets'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['assets']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['allAssets']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['assets']->key => $_smarty_tpl->tpl_vars['assets']->value) {
$_smarty_tpl->tpl_vars['assets']->_loop = true;
?>
                            <tr>
                                <td><?php echo $_smarty_tpl->tpl_vars['assets']->value['asset_id'];?>
</td>
                                <td><?php if ($_smarty_tpl->tpl_vars['assets']->value['file_type']=='image') {?><a href="<?php echo $_smarty_tpl->tpl_vars['ASSETS_PATH_DIR']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['assets']->value['local_file_name'];?>
" target="_blank"><img src="<?php echo $_smarty_tpl->tpl_vars['ASSETS_PATH_DIR']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['assets']->value['local_file_name'];?>
" width="20" alt="<?php echo $_smarty_tpl->tpl_vars['assets']->value['file_description'];?>
"></a><?php } else { ?><a href="<?php echo $_smarty_tpl->tpl_vars['ASSETS_PATH_DIR']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['assets']->value['local_file_name'];?>
" target="_blank">View File</a><?php }?></td>
                                <td><?php echo $_smarty_tpl->tpl_vars['assets']->value['public_name'];?>
</td>
                                <td><?php echo $_smarty_tpl->tpl_vars['assets']->value['file_type'];?>
</td>
                                <td><?php echo $_smarty_tpl->tpl_vars['assets']->value['file_size'];?>
</td>
                                <td><?php echo $_smarty_tpl->tpl_vars['assets']->value['image_width'];?>
 x <?php echo $_smarty_tpl->tpl_vars['assets']->value['image_height'];?>
</td>
                                <td><?php echo $_smarty_tpl->tpl_vars['assets']->value['file_description'];?>
</td>
                                <td><?php echo $_smarty_tpl->tpl_vars['assets']->value['creation_date'];?>
</td>
                                <td width="100">
                                    <button name="select_asset" onclick="select_asset(this);" class="select_asset btn btn-default" url-path="<?php echo $_smarty_tpl->tpl_vars['ASSETS_PATH_DIR']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['assets']->value['local_file_name'];?>
" img-alt="<?php echo $_smarty_tpl->tpl_vars['assets']->value['file_description'];?>
">Select asset</button>
                                </td>
                            </tr>
                            <?php }
if (!$_smarty_tpl->tpl_vars['assets']->_loop) {
?>
                            <tr>
                                <td colspan="7" class="text-center">No results</td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>

                <?php if ($_smarty_tpl->tpl_vars['no_of_page']->value>0) {?>
                    <div class="paginare  bottom">
                        <ul class="list_pagination">
                            <li>
                                <a  href="<?php echo $_smarty_tpl->tpl_vars['link_pagination']->value;?>
#<?php echo $_smarty_tpl->tpl_vars['link_ajax']->value;?>
pagina=1" class="to_left_left" onclick="pagination_assets('1', 'browser', '<?php echo $_smarty_tpl->tpl_vars['assets_type']->value;?>
');return false;"><span aria-hidden="true" class="glyphicon glyphicon-step-backward"></span></a>
                            </li>
                            <li><a  href="<?php echo $_smarty_tpl->tpl_vars['link_pagination']->value;?>
#<?php echo $_smarty_tpl->tpl_vars['link_ajax']->value;?>
pagina=<?php echo $_smarty_tpl->tpl_vars['page_no']->value-1;?>
" class="to_left" onclick="javascript:prev_assets('browser', '<?php echo $_smarty_tpl->tpl_vars['assets_type']->value;?>
');return false;"><span aria-hidden="true" class="glyphicon glyphicon-chevron-left"></span></a></li>
                            <?php $_smarty_tpl->tpl_vars['start'] = new Smarty_Variable;$_smarty_tpl->tpl_vars['start']->step = 1;$_smarty_tpl->tpl_vars['start']->total = (int) ceil(($_smarty_tpl->tpl_vars['start']->step > 0 ? $_smarty_tpl->tpl_vars['no_of_page']->value+1 - (1) : 1-($_smarty_tpl->tpl_vars['no_of_page']->value)+1)/abs($_smarty_tpl->tpl_vars['start']->step));
if ($_smarty_tpl->tpl_vars['start']->total > 0) {
for ($_smarty_tpl->tpl_vars['start']->value = 1, $_smarty_tpl->tpl_vars['start']->iteration = 1;$_smarty_tpl->tpl_vars['start']->iteration <= $_smarty_tpl->tpl_vars['start']->total;$_smarty_tpl->tpl_vars['start']->value += $_smarty_tpl->tpl_vars['start']->step, $_smarty_tpl->tpl_vars['start']->iteration++) {
$_smarty_tpl->tpl_vars['start']->first = $_smarty_tpl->tpl_vars['start']->iteration == 1;$_smarty_tpl->tpl_vars['start']->last = $_smarty_tpl->tpl_vars['start']->iteration == $_smarty_tpl->tpl_vars['start']->total;?>
                                <?php if ($_smarty_tpl->tpl_vars['start']->value>=$_smarty_tpl->tpl_vars['min_page']->value&&$_smarty_tpl->tpl_vars['start']->value<=$_smarty_tpl->tpl_vars['max_page']->value) {?>
                                    <li <?php if ($_smarty_tpl->tpl_vars['start']->value==$_smarty_tpl->tpl_vars['page_no']->value) {?> class="curent_item"<?php }?>>
                                        <a   href="<?php echo $_smarty_tpl->tpl_vars['link_pagination']->value;?>
#<?php echo $_smarty_tpl->tpl_vars['link_ajax']->value;?>
pagina=<?php echo $_smarty_tpl->tpl_vars['start']->value;?>
" class="pagination_item <?php if ($_smarty_tpl->tpl_vars['start']->value==$_smarty_tpl->tpl_vars['page_no']->value) {?> curent_item<?php }?>" id="<?php echo $_smarty_tpl->tpl_vars['start']->value;?>
botom" onclick="pagination_assets('<?php echo $_smarty_tpl->tpl_vars['start']->value;?>
', 'browser', '<?php echo $_smarty_tpl->tpl_vars['assets_type']->value;?>
');return false;"><?php echo $_smarty_tpl->tpl_vars['start']->value;?>
</a>
                                    </li>
                                <?php }?>
                            <?php }} ?>
                            <li>
                                <a  href="<?php echo $_smarty_tpl->tpl_vars['link_pagination']->value;?>
#<?php echo $_smarty_tpl->tpl_vars['link_ajax']->value;?>
pagina=<?php echo $_smarty_tpl->tpl_vars['page_no']->value+1;?>
" class="to_right" onclick="javascript:next_assets('browser', '<?php echo $_smarty_tpl->tpl_vars['assets_type']->value;?>
');return false;"><span aria-hidden="true" class="glyphicon glyphicon-chevron-right"></span></a></li>
                            <li><a  href="<?php echo $_smarty_tpl->tpl_vars['link_pagination']->value;?>
#<?php echo $_smarty_tpl->tpl_vars['link_ajax']->value;?>
pagina=<?php echo $_smarty_tpl->tpl_vars['no_of_page']->value;?>
" class="to_right_right" onclick="pagination_assets('<?php echo $_smarty_tpl->tpl_vars['no_of_page']->value;?>
', 'browser', '<?php echo $_smarty_tpl->tpl_vars['assets_type']->value;?>
');return false;"><span aria-hidden="true" class="glyphicon glyphicon-step-forward"></span></a></li>
                        </ul>
                    </div>
                <?php }?>

            </div>
        </div>

    </div>

</div>
<?php echo '<script'; ?>
>
    //var source = '<?php echo $_smarty_tpl->tpl_vars['source']->value;?>
';
    //var input_file_id = '<?php echo $_smarty_tpl->tpl_vars['input_file_url']->value;?>
';
    var source = '';
    var input_file_id = '<?php echo $_smarty_tpl->tpl_vars['input_file_url']->value;?>
';
<?php echo '</script'; ?>
>

    <?php echo '<script'; ?>
>
        function select_asset(element)
        {
            if (source == '' || typeof source == 'undefined') {
                source = getUrlParameter('source');
            }
            if (input_file_id == '') {
                input_file_id = getUrlParameter('input_file_url');
            }
            // console.log(source);
            if (source == '' || typeof source == 'undefined')
            {
                source = 'ckeditor';
            }
            // console.log(source);
            // console.log(input_file_id);

            var fileSrc = $(element).attr("url-path");
            var fileSrcAlt = $(element).attr("img-alt");
            if (source == 'ckeditor')
            {
                var CKEditorFuncNum =$("#CKEditorFuncNum").val();
                window.opener.CKEDITOR.tools.callFunction(CKEditorFuncNum, fileSrc);
                window.opener.$("label:contains('Alternative Text')").next().find("input[class='cke_dialog_ui_input_text']").val(fileSrcAlt);
            }
            else
            {

                // Set them to the parent window
                window.opener.$("#"+input_file_id).val(fileSrc);
                if (input_file_id == 'edit_form_video_image_url') // need to set a preview for Edit Form Fields
                {
                    window.opener.$("#edit_form_video_image_url_preview").attr('src',fileSrc);
                }
            }

            // Close the popup
            window.close();
        }
        /*  $(document).ready(function()
         {
         $( ".select_asset" ).click(function() {

         //select_asset($(this));

         console.log(source);
         console.log(input_file_id);

         if (source == '') {
         source = getUrlParameter('source');
         }
         if (input_file_id == '') {
         input_file_id = getUrlParameter('input_file_url');
         }
         if (source == '')
         {
         source == 'ckeditor';
         }
         console.log(source);
         console.log(input_file_id);

         var fileSrc = $(this).attr("url-path");
         var fileSrcAlt = $(this).attr("img-alt");
         if (source == 'ckeditor')
         {
         var CKEditorFuncNum =$("#CKEditorFuncNum").val();
         window.opener.CKEDITOR.tools.callFunction(CKEditorFuncNum, fileSrc);
         window.opener.$("label:contains('Alternative Text')").next().find("input[class='cke_dialog_ui_input_text']").val(fileSrcAlt);
         }
         else
         {

         // Set them to the parent window
         window.opener.$("#"+input_file_id).val(fileSrc);
         if (input_file_id == 'edit_form_video_image_url') // need to set a preview for Edit Form Fields
         {
         window.opener.$("#edit_form_video_image_url_preview").attr('src',fileSrc);
         }
         }

         // Close the popup
         //window.close();

         });
         });*/

    <?php echo '</script'; ?>
>

</body>
</html>
<?php }} ?>
