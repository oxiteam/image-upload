# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up

* First of all, in order to use Authorize.net api the server must be properly configured and according to pcicompliance.(http://www.authorize.net/resources/pcicompliance/)
* 
* Neccessary files and folders:
* api-payment-master/
* api-payment-master/constants/Constants.php (here must configure constants for
* MERCHANT_LOGIN_ID
* and 
* MERCHANT_TRANSACTION_KEY
* , provided by customer and for the api to be ready to use)
* in main.php file must (generally created with composer, in console)
* require 'api-payment-master/vendor/autoload.php';
* 
* To use the class 'payment' copy secure.pay.class.php and include it in inc/ directory. (It also uses globals for smarty and sanitation)
* 
* The use of it is quite simple, once all configuration is done:
* $new_payment = new payment();
* $new_payment->chargeCreditCard($value);
* 
* The form has 3 required name inputs:
* x_card_num = card number ()
* x_exp_date = expiration date
* x_card_code = Card Verification Value or CVV
* 
* the class itself catches this card information and put it in an array to use in the chargeCreditCard() function.
* 
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact