{include file="{$tpl_folder}/header.tpl"}                                                                                                                                                                                                                                                                                                                                                          
<div class="image-form-wrapper">
  <div class="row">
    <div class="col-lg-4">
    {if $action eq 'start_payment'}
      {if !empty($output)}
      <div class="btn-success" style="padding:10px;">{$output}</div>
      {/if}
      {if !empty($errors)}
      <div class="btn-danger" style="padding:10px;">{$errors}</div>
      {/if}
      <a href="test2.php" class="form-control">Back to form</a>
    {else}




      <form action="" method="POST">

        <div class="form-group">
          <label for="x_card_num">Card Number <img src="content/V.gif" width="43" height="26" title="Visa" alt="Visa"><img src="content/MC.gif" width="41" height="26" title="MasterCard" alt="MasterCard"><img src="content/Amex.gif" width="40" height="26" title="American Express" alt="American Express"><img src="content/Disc.gif" width="40" height="26" title="Discover" alt="Discover"><img src="content/JCB.gif" width="21" height="26" title="JCB" alt="JCB"></label>
          <input type="text" class="form-control" id="x_card_num" name="x_card_num" maxlength="16" autocomplete="off" value="" placeholder="0000000000000000" required aria-required="true" aria-invalid="false">*
          <span class="Comment">(enter number without spaces or dashes)</span>
        </div>

        <div class="row">

          <div class="col-lg-6">
            <div class="form-group">
              <label for="x_exp_date">Expiration Date</label>
              <input type="text" class="form-control" id="x_exp_date" name="x_exp_date" maxlength="4" autocomplete="off" value="" required aria-required="true" aria-invalid="false">*
              <span class="Comment">(mmyy)</span>
            </div>
          </div>

          <div class="col-lg-6">
            <div class="form-group">
              <label for="x_card_code">CVV (Card Verification Value). </label>
              <input type="text" class="form-control" id="x_card_code" name="x_card_code" maxlength="4" autocomplete="off" value="" required aria-required="true" aria-invalid="false">*
              <span class="Comment">(enter number without spaces or dashes) <a href="https://www.cvvnumber.com/cvv.html" target="_blank" >What is CCV?</a></span>
            </div>
          </div>

        </div>

        <div class="form-group">
          <input type="hidden" name="action" value="start_payment" />
          <input type="submit" id="btnSubmit" class="form-control" value="Pay Now" onclick="return onSubmit();">
        </div>

      </form>


    {/if}
    </div>
  </div>
</div>
