<div class="clear"> </div>

	<div class="content">
        <div class="left-footer">
            <ul class="meniu-footer" id="admin_top_menu">
                <li {if $action=='admin-errors'}class="active"{/if}><a href="{$root_url_admin}" >Errors Log </a></li>
                <li {if $action=='admin-users'}class="active"{/if}><a href="{$admin_users_url}" >Administrators</a></li>

                <!--<li {if $action=='admin-categories'}class="active"{/if}><a href="{$admin_categories_url}" >Categories</a></li>-->
                <!--<li {if $action=='admin-articles'}class="active"{/if}><a href="{$admin_articles_url}" >Articles</a></li>-->

                <li {if $action =='admin-assets'}class="active"{/if}><a href="{$admin_assets_url}" >Assets</a></li>

                <li {if $action =='admin-emails_management'}class="active"{/if}><a href="{$admin_emails_management_url}" >Emails</a></li>
 
                <li {if $action =='admin-settings'}class="active"{/if}><a href="{$admin_settings_url}" >Settings</a></li>


                <!--<li style="float: right;"><a href="{$admin_users_logout_url}" >Logout</a></li>-->
            </ul>
        </div>
	</div>

</div>
<div class="clear"> </div>
