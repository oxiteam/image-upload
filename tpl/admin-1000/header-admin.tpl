<!doctype html>
<html>
<head>
    <title>Admin CMS MS</title>

    <meta name="viewport" content="width=1024" />
    <meta charset="utf-8" />

    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet" >
    <link href="{$root_url}/css/style.css" rel="stylesheet" type="text/css" />
    <link href="{$root_url}/admin-1000/css/style_admin.css" rel="stylesheet" type="text/css" />
    <!--<link href="{$root_url}/admin-1000/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />-->
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/themes/smoothness/jquery-ui.css">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>

    <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>

    <!--<script src="{$root_url}/admin-1000/js/bootstrap-datepicker.min.js"></script>-->

    <script src="{$root_url}/admin-1000/js/main_admin.js"></script>

</head>
<body>
