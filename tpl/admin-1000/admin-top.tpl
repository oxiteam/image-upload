<header class="master-header" >
    <a href="{$root_url}">
      <h1>
     {$site_logo}{$site_name}
   </h1>
    </a>
  <a href="{$root_url_admin}"><h3>Admin</h3></a>
    <div class="header-icons">
        {if $is_admin}
        <span class="ic-search"></span>
        <div id="meniu-top" >
            <ul class="meniu">
                <li>
                    {$SESSION.user_name}, <a href="mailto:{$SESSION.email}">{$SESSION.email}</a><small></small>
                </li>
                <li style="float: right;"><a href="{$admin_users_logout_url}" >Logout</a></li>
            </ul>
        </div>
        {/if}
    </div>

</header>

<div class="content-wrap top-menu">
    {if $is_admin}
    {include file="{$tpl_folder}/admin-1000/admin-menu.tpl"}
    {/if}
</div>
