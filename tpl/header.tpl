<!doctype html>
<html lang="ro" itemscope="itemscope" >
<head>
    <title>{$page_title}</title>

    <meta name="viewport" content="width=1024" />
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet" >
    <link href="{$root_url}/css/style.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/themes/smoothness/jquery-ui.css">

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
    <script type="text/javascript" src="https://www.google.com/recaptcha/api.js"> </script>

    <script>
        //var sitekey = '{$sitekey}';
        var root_url = '{$root_url}';
    </script>

    <script src="{$root_url}/js/main.js"></script>
    <script src="{$root_url}/js/paymentform.js"></script>

</head>

{include file="{$tpl_folder}/top.tpl"}
