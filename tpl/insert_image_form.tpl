<div class="image-form-wrapper">

  <form action="" method="post" id="imgupload" name="imgupload" enctype="multipart/form-data">

    <div class="form-group">

      <label for="file">Upload Image here</label>

      <input form="imgupload" class="form-control" type="file" id="file" name="file" required  />

    </div>

    <div class="form-group">

      <label for="file_name">File name</label>

      <input form="imgupload" class="form-control" type="text" id="file_name" name="file_name" required value="{$file_name}" placeholder="Ex: my_foto(.jpg)" />

    </div>

    <div class="form-group">

      <label for="description">Description</label>

      <textarea form="imgupload" class="form-control" type="text" id="description" name="description" required  >{$description}</textarea>

    </div>

    <div class="form-group">

      <div class="g-recaptcha" data-sitekey="{$sitekey}" id="recaptha_id_4comment" form="imgupload" style="width:100%"></div>

    </div>

    <div class="form-group">

      <input type="hidden" name="action" value="add_image" />

      <input form="imgupload" class="form-control" type="submit" id="description"  required value="Submit photo" />

    </div>

  </form>

</div>

<div class="error-section">
  <ul>
  {foreach from=$errors item=err}
      <li>{$err}</li>
  {/foreach}
  </ul>
</div>
