<?php
require('inc/config.php');
#require('inc/Users.class.php');

$action = filter_var($_GET ['action'], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
$user_email = filter_var($_REQUEST ['user_email'], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
$user_password = filter_var($_REQUEST ['user_password'], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);

$present = time();
$action = $_REQUEST ['action'];

//load all classes
$login = new loginAdmin();
$Users = new Users();
$Sanitation = new Sanitation();
$FormatDate = new FormatDate();
$error_handler = new ErrorHandler();
# $this_site = new Site_settings($config['THIS_SITE_ID']);
$this_site = new Site_settings(1);
#$css_counter = $this_site->fetch_css_counter();
#$smarty->assign('css_counter', $css_counter);
#$js_counter = $this_site->fetch_js_counter();
#$smarty->assign('js_counter', $js_counter);

#action
switch ($action)
{
    case 'admin-1000':
    case 'admin-login':
    case 'admin-users':
    case 'admin-add_admin':
    case 'admin-edit_admin':
    case 'admin-delete_user':
    case 'admin-logout':
    case 'admin-errors':
    case 'admin-comments':
    case 'admin-comments_unmoderated':
    case 'admin-emails_management':
    case 'admin-settings':
    case 'show_404':
        break;
    default:
        $action = 'admin-1000';
}

// Read data for action insert-thread
$form_data['user_name'] = $Sanitation->remove_html($_REQUEST['user_name'], true);
$form_data['email'] = $purifier->purify($_REQUEST['email']);
$form_data['user_type'] = $Sanitation->remove_html($_REQUEST['user_type']);
$smarty->assign('form_data',$form_data);

$is_user_logged_in = $login->check_auth_state();
if (!$is_user_logged_in)
{
    // The user cannot use the admin interface if he/she its not logged in;
    $is_logged = $login->login_user($user_email, $user_password);
    if (!$is_logged)
    {
        $smarty->display($tpl_folder_admin.'/index-admin.tpl');
        exit();
    }
    else
    {
        header("Location: $admin_errors_url");
    }
}



$is_admin = $Users->check_if_user_is_admin($_SESSION['user_id']);
if (!$is_admin)
{
    $login->logout();
    exit();
}
$smarty->assign('SESSION',$_SESSION);

$users_types_list = $Users->fetch_users_types();
$smarty->assign('users_types_list',$users_types_list);


$smarty->assign('is_admin',$is_admin);
$smarty->assign('action',$action);


if ($action=="show_404")
{
    header('HTTP/1.0 404 Not Found');
    $smarty->assign('error_description', 'Ne pare rau, aceasta discutie nu exista pe Comunitatea HemoStop.<br/>Va recomandam sa utilizati functia de cautare:');
    $smarty->display('index-error.tpl');
    die();
}

#$Comments = new guestbook();
#$unmoderatedComments = $Comments -> count_comments(' mode = 0 ');
#$smarty->assign('unmoderatedComments',$unmoderatedComments);



/* ADMIN SECTION */
if ($action == 'admin-1000')
{

    if ($is_admin)
    {
        $limit = $config['CT__NUMBER_OF_ERRORS_PER_PAGE'];

        $allErrors = $error_handler -> get_all_errors(0,$limit);

        $total_results =  $error_handler->count_all_errors();

        if ($total_users > 0)
        {
            $nr_of_page=ceil($total_users/$limit);
        }
        else
        {
            $nr_of_page=0;
        }

        $smarty->assign('s_page',1);
        $smarty->assign('no_of_page',$nr_of_page);
        $smarty->assign('allErrors',$allErrors);
        $smarty->assign("error", '');
        $smarty->display($tpl_folder.'/admin-1000/admin_errors.tpl');
    }
    else
    {
        $smarty->display($tpl_folder.'/admin-1000/index-admin.tpl');
    }

}
if ($action == 'admin-login')
{

    $form_data = array();
    $form_data['email']= strip_tags($_REQUEST['admin_email']);
    $form_data['password'] = $Sanitation->remove_html($_REQUEST['password'], true);



    $validate_form = $Login -> validate_admin_login($form_data);
    if (empty($validate_form))
    {
        $_SESSION['email'] =  $form_data['email'] ;
        $_SESSION['user_type'] =  'admin' ;

        $userInfo = $Users -> fetch_user_by_email ($_SESSION['email']);

        //update IP for logged user
        $userInfo = $Users -> fetch_user_by_email($_SESSION['email']);
        $userData = array('user_id'=>$userInfo['user_id'], 'ip_address' => $_SERVER['REMOTE_ADDR']);
        $Users -> update_user($userData);

        header("Location: $admin_errors_url");
    }
    else
    {
        $smarty->assign("error", implode('',$validate_form));
        $smarty->assign("email",$form_data['email']) ;
        $smarty->display($tpl_folder_admin.'/index-admin.tpl');
    }

}
if ($action == 'admin-logout')
{

    $login->logout();
}
if ($action == 'admin-delete_user') {

    if (!$is_admin) {

        header("Location: $root_url_admin");
        die();
    }
    $user_id =  abs(intval($_REQUEST['user_id']));
    $user_id = abs(intval($user_id));
    $Users -> delete_user($user_id);

    exit();
}

if ($action == 'admin-users') {

    if (!$is_admin) {

        header("Location: $root_url_admin");
        die();
    }

    $users_types_list = $Users->fetch_users_types();
    $smarty->assign('users_types_list',$users_types_list);

    $smarty->assign('usersInfo', $usersInfo);

    $where = array();
    $get_data = array();
    $get_data['s_name'] = $Sanitation->remove_html($_REQUEST['s_name'], true);
    $get_data['s_email'] = $Sanitation->remove_html($_REQUEST['s_email'], true);
    $get_data['s_ip'] = $Sanitation->remove_html($_REQUEST['s_ip'], true);
    $get_data['s_date_start'] = $Sanitation->remove_html($_REQUEST['s_date_start'], true);
    $get_data['s_date_end'] = $Sanitation->remove_html($_REQUEST['s_date_end'], true);
    $get_data['s_type'] = $Sanitation->remove_html($_REQUEST['s_type'], true);
    #var_dump($get_data);
    if ($get_data['s_name'] != '') {
        $where[] = " AND user_name LIKE '%".$Sanitation->prepare_for_database($get_data['s_name'])."%' " ;
        $searched = true;
    }
    if ($get_data['s_email'] != '') {
        $where[] = " AND email LIKE '%".$Sanitation->prepare_for_database($get_data['s_email'])."%' " ;
        $searched = true;
    }
    if ($get_data['s_ip'] != '') {
        $where[] = " AND ip_address LIKE '%".$Sanitation->prepare_for_database($get_data['s_ip'])."%' " ;
        $searched = true;
    }
    if ($get_data['s_date_start'] != '') {
        $where[] = " AND created_date >= '".date("Y-m-d", strtotime($get_data['s_date_start']))." 00:00:00' " ;
        $searched = true;
    }
    if ($get_data['s_date_end'] != '') {
        $where[] = " AND created_date <= '".date("Y-m-d", strtotime($get_data['s_date_end']))." 23:59:59' " ;
        $searched = true;
    }
    if ($get_data['s_type'] != '') {
        $where[] = " AND user_type = '".$get_data['s_type']."' " ;
        $searched = true;
    }

    $start = 0;
    $smarty->assign('searched',$searched);
    $smarty->assign('s_name',$get_data['s_name']);
    $smarty->assign('s_email',$get_data['s_email']);
    $smarty->assign('s_ip',$get_data['s_ip'] );
    $smarty->assign('s_date_start',$get_data['s_date_start']);
    $smarty->assign('s_date_end',$get_data['s_date_end']);
    $smarty->assign('s_type',$get_data['s_type']);
    $smarty->assign('start',$start);

    $where[] = " AND user_type = '1' ";
    $allUsersDB = $Users -> fetch_users($start,$limit,'created_date','desc', implode(' ', $where));
    $total_users =  $Users -> count_users(implode(' ', $where));

    $allUsers = array();
    foreach ($allUsersDB as $key => $user)
    {
        $usersInfo[$key] = $user;
        $usersInfo[$key]['created_date'] =  $FormatDate -> long_format($user['created_date']);
    }

    $smarty->assign('allUsers',$allUsers);

    $limit = $config['CT_NUMBER_OF_USERS_PER_PAGE'];
    $page = 1;

    if ($total_users > 0)
    {
        $nr_of_page=ceil($total_users/$limit);
    }
    else
    {
        $nr_of_page=0;
    }

    $min_no = $page - $config['CT__NUMBER_OF_PAGINATIONS_NUMBER'];
    $min_page = ($min_no <= 0 )? 1 : $min_no;

    $max_no = $page + $config['CT__NUMBER_OF_PAGINATIONS_NUMBER'];
    $max_page = ($max_no >= $nr_of_page )? $nr_of_page : $max_no;

    $smarty->assign('page_no',$page);
    $smarty->assign('no_of_page',$nr_of_page);
    $smarty->assign('type','admin');
    $smarty->assign('current_admins', 1);
    $smarty->assign('link_pagination', $link_pagination);
    $smarty->assign('link_ajax', $link_ajax);
    $smarty->assign('min_page',$min_page);
    $smarty->assign('max_page',$max_page);
    //$allUsers = $Users -> fetch_admin_users();
    $smarty->assign('allUsers',$allUsers);
    $smarty->assign("error", '');
    $smarty->display($tpl_folder.'/admin-1000/admin_users.tpl');
}

if ($action == 'admin-errors') {

    if (!$is_admin) {

        header("Location: $root_url_admin");
        die();
    }



    $limit = $config['CT__NUMBER_OF_ERRORS_PER_PAGE'];

    $allErrors = $error_handler -> get_all_errors(0,$limit);

    $total_results =  $error_handler->count_all_errors();
    if ($total_results > 0)
    {
        $nr_of_page=ceil($total_results/$limit);
    } else {
        $nr_of_page=0;
    }


    $smarty->assign('s_page',1);
    $smarty->assign('no_of_page',$nr_of_page);
    $smarty->assign('allErrors',$allErrors);
    $smarty->assign("error", '');
    $smarty->display($tpl_folder.'/admin-1000/admin_errors.tpl');
}

if ($action == 'admin-add_admin') {

    if (!$is_admin) {

        header("Location: $root_url_admin");
        die();
    }


    $users_types_list = $Users->fetch_users_types();
    $smarty->assign('users_types_list',$users_types_list);

    $allUsers = $Users -> fetch_admin_users();

    $form_data = array();
    $form_data['user_name'] = $Sanitation->remove_html($_REQUEST['user_name'], true);
    $form_data['email']= strip_tags($_REQUEST['email']);
    $form_data['sex']= strip_tags($_REQUEST['sex']);
    $form_data['password'] = $Sanitation->remove_html($_REQUEST['password'], true);
    $form_data['user_type'] = $Sanitation->remove_html($_REQUEST['user_type'], true);
    $form_data['action_type']= 'add';

    $validate_form = $Users->validate_user_admin($form_data);

    if (empty($validate_form))
    {

        $user_id = $Users -> add_user($form_data);
        $allUsers = $Users -> fetch_users();
        $smarty->assign("error", '');

        header("Location: $admin_users_url");
    }
    else
    {
        $smarty->assign("error", implode('',$validate_form));
        $smarty->assign("form_data", $form_data);
    }
    $where = array();
    $get_data = array();
    $get_data['s_name'] = $Sanitation->remove_html($_REQUEST['s_name'], true);
    $get_data['s_email'] = $Sanitation->remove_html($_REQUEST['s_email'], true);
    $get_data['s_ip'] = $Sanitation->remove_html($_REQUEST['s_ip'], true);
    $get_data['s_date_start'] = $Sanitation->remove_html($_REQUEST['s_date_start'], true);
    $get_data['s_date_end'] = $Sanitation->remove_html($_REQUEST['s_date_end'], true);
    $get_data['s_type'] = $Sanitation->remove_html($_REQUEST['s_type'], true);


    if ($get_data['s_name'] != '') {
        $where[] = " AND user_name LIKE '%".$Sanitation->prepare_for_database($get_data['s_name'])."%' " ;
        $searched = true;
    }
    if ($get_data['s_email'] != '') {
        $where[] = " AND email LIKE '%".$Sanitation->prepare_for_database($get_data['s_email'])."%' " ;
        $searched = true;
    }
    if ($get_data['s_ip'] != '') {
        $where[] = " AND ip_address LIKE '%".$Sanitation->prepare_for_database($get_data['s_ip'])."%' " ;
        $searched = true;
    }
    if ($get_data['s_date_start'] != '') {
        $where[] = " AND created_date >= '".date("Y-m-d", strtotime($get_data['s_date_start']))." 00:00:00' " ;
        $searched = true;
    }
    if ($get_data['s_date_end'] != '') {
        $where[] = " AND created_date <= '".date("Y-m-d", strtotime($get_data['s_date_end']))." 23:59:59' " ;
        $searched = true;
    }
    if ($get_data['s_type'] != '') {
        $where[] = " AND user_type = '".$get_data['s_type']."' " ;
        $searched = true;
    }

    $smarty->assign('searched',$searched);
    $smarty->assign('s_name',$get_data['s_name']);
    $smarty->assign('s_email',$get_data['s_email']);
    $smarty->assign('s_ip',$get_data['s_ip'] );
    $smarty->assign('s_date_start',$get_data['s_date_start']);
    $smarty->assign('s_date_end',$get_data['s_date_end']);
    $smarty->assign('s_type',$get_data['s_type']);

    $allUsers = $Users -> fetch_admin_users(0, $config['CT_NUMBER_OF_USERS_PER_PAGE'],'created_date','desc', implode(' ', $where));
    $where[] = " AND user_type = '1'  ";
    $total_users =  $Users -> count_users(implode(' ', $where));
    $smarty->assign('allUsers',$allUsers);

    $limit = $config['CT_NUMBER_OF_USERS_PER_PAGE'];
    $page = 1;

    if ($total_users > 0)
    {
        $nr_of_page=ceil($total_users/$limit);
    }
    else
    {
        $nr_of_page=0;
    }

    $min_no = $page - $config['CT__NUMBER_OF_PAGINATIONS_NUMBER'];
    $min_page = ($min_no <= 0 )? 1 : $min_no;

    $max_no = $page + $config['CT__NUMBER_OF_PAGINATIONS_NUMBER'];
    $max_page = ($max_no >= $nr_of_page )? $nr_of_page : $max_no;

    $smarty->assign('page_no',$page);
    $smarty->assign('no_of_page',$nr_of_page);
    $smarty->assign('type','admin');
    $smarty->assign('current_admins', 1);
    $smarty->assign('link_pagination', $link_pagination);
    $smarty->assign('link_ajax', $link_ajax);
    $smarty->assign('min_page',$min_page);
    $smarty->assign('max_page',$max_page);
    //$allUsers = $Users -> fetch_admin_users();
    $smarty->assign('allUsers',$allUsers);
    $smarty->display($tpl_folder_admin.'/admin_users.tpl');
}

if ($action == 'admin-edit_admin') {

    if (!$is_admin) {

        header("Location: $root_url_admin");
        die();
    }
    /**/

    $users_types_list = $Users->fetch_users_types();
    $smarty->assign('users_types_list',$users_types_list);

    $allUsers = $Users -> fetch_admin_users();

    $form_data = array();
    $form_data['user_name'] = $Sanitation->remove_html($_REQUEST['user_name'], true);
    $form_data['email']= strip_tags($_REQUEST['email']);
    $form_data['sex']= strip_tags($_REQUEST['sex']);
    $form_data['password'] = $Sanitation->remove_html($_REQUEST['password'], true);
    $form_data['user_type'] = $Sanitation->remove_html($_REQUEST['user_type'], true);
    $form_data['action_type']= 'edit';
    $form_data['user_id'] = abs(intval($_REQUEST['user_id']));
    $form_data['page_no'] = abs(intval($_REQUEST['page_no']));

    $validate_form = $Users->validate_user_admin($form_data);

    if (empty($validate_form))
    {

        $user_id = $Users -> update_user($form_data);
        $allUsers = $Users -> fetch_admin_users();
        $smarty->assign("error", '');

        $page_no = $form_data['page_no'];
        header("Location: $admin_users_url#pagina=$page_no");
    }
    else
    {
        $smarty->assign("error", implode('',$validate_form));
        $smarty->assign("form_data", $form_data);
    }
    $smarty->assign('allUsers',$allUsers);
    $smarty->display($tpl_folder_admin.'/admin_users.tpl');
    //$smarty->display($tpl_folder_admin.'/index-admin.tpl');
}

if ($action == 'admin-comments') {

    if (!$is_admin) {

        header("Location: $root_url_admin");
        die();
    }
    #$Comments = new guestbook();
    $where = array();
    #$get_data = array();

    $allComments = $Comments -> fetch_comments(0, $config['CT_NUMBER_OF_USERS_PER_PAGE'],'timestamp','desc', implode(' ', $where));
    $total_comments =  $Comments -> count_comments(implode(' ', $where));

    $limit = $config['CT_NUMBER_OF_USERS_PER_PAGE'];
    $page = 1;

    if ($total_users > 0)
    {
        $nr_of_page=ceil($total_comments/$limit);
    }
    else
    {
        $nr_of_page=0;
    }

    $min_no = $page - $config['CT__NUMBER_OF_PAGINATIONS_NUMBER'];
    $min_page = ($min_no <= 0 )? 1 : $min_no;

    $max_no = $page + $config['CT__NUMBER_OF_PAGINATIONS_NUMBER'];
    $max_page = ($max_no >= $nr_of_page )? $nr_of_page : $max_no;

    $smarty->assign('page_no',$page);
    $smarty->assign('no_of_page',$nr_of_page);
    $smarty->assign('type','admin');
    $smarty->assign('current_comments', 1);
    $smarty->assign('link_pagination', $link_pagination);
    $smarty->assign('link_ajax', $link_ajax);
    $smarty->assign('min_page',$min_page);
    $smarty->assign('max_page',$max_page);
    $smarty->assign('allComments',$allComments);
    $smarty->assign("error", '');
    $smarty->display($tpl_folder_admin.'/admin_comments.tpl');
}

if ($action == 'admin-comments_unmoderated') {

    if (!$is_admin) {
        header("Location: $root_url_admin");
        die();
    }

    #$Comments = new guestbook();

    $where = array();
    $where[] = ' mode = 0';

    $allComments = $Comments -> fetch_comments(0, $config['CT_NUMBER_OF_USERS_PER_PAGE'],'timestamp','asc', implode(' ', $where));
    $total_comments =  $Comments -> count_comments(implode(' ', $where));

    $limit = $config['CT_NUMBER_OF_USERS_PER_PAGE'];
    $page = 1;

    if ($total_users > 0)
    {
        $nr_of_page=ceil($total_comments/$limit);
    }
    else
    {
        $nr_of_page=0;
    }

    $min_no = $page - $config['CT__NUMBER_OF_PAGINATIONS_NUMBER'];
    $min_page = ($min_no <= 0 )? 1 : $min_no;

    $max_no = $page + $config['CT__NUMBER_OF_PAGINATIONS_NUMBER'];
    $max_page = ($max_no >= $nr_of_page )? $nr_of_page : $max_no;

    $smarty->assign('page_no',$page);
    $smarty->assign('no_of_page',$nr_of_page);
    $smarty->assign('type','admin');
    $smarty->assign('current_comments', 1);
    $smarty->assign('link_pagination', $link_pagination);
    $smarty->assign('link_ajax', $link_ajax);
    $smarty->assign('min_page',$min_page);
    $smarty->assign('max_page',$max_page);
    $smarty->assign('allComments',$allComments);
    $smarty->assign("error", '');
    $smarty->display($tpl_folder_admin.'/admin_comments_unmoderated.tpl');
}

if ($action == 'admin-emails_management'){

    $page_title = 'Manage the emails management at CMS MS';
    $smarty->assign('page_title', $page_title);

    $limit = $config['CT_NUMBER_OF_EMAILS_PER_PAGE'];
    $page = 1;

    $Email = new emails_management();

    $where = array();
    $emails_db = $Email -> fetch_emails_management(0,$limit, implode(' ', $where));
    $total_emails =  $Email -> count_emails_management(implode(' ', $where));

    $emailsList = array();
    foreach ($emails_db as $key => $emails)
    {
        $emailsList[$key] = $emails;
        $emailsList[$key]['creation_date'] =  $FormatDate -> long_format($emails['creation_date']);
        $emailsList[$key]['last_updated_date'] = ($emails['last_updated_date'] != '0000-00-00 00:00:00')? $FormatDate -> long_format($emails['last_updated_date']) : $FormatDate -> long_format($emails['creation_date']);
    }

    if ($total_emails > 0)
    {
        $nr_of_page=ceil($total_emails/$limit);
    }
    else
    {
        $nr_of_page=0;
    }

    $min_no = $page - $config['CT__NUMBER_OF_PAGINATIONS_NUMBER'];
    $min_page = ($min_no <= 0 )? 1 : $min_no;

    $max_no = $page + $config['CT__NUMBER_OF_PAGINATIONS_NUMBER'];
    $max_page = ($max_no >= $nr_of_page )? $nr_of_page : $max_no;
    #var_dump(ceil($total_emails/$limit));
    $smarty->assign('page_no',$page);
    $smarty->assign('no_of_page',$nr_of_page);
    $smarty->assign('current_emails', 1);
    $smarty->assign('link_pagination', $link_pagination);
    $smarty->assign('link_ajax', $link_ajax);
    $smarty->assign('min_page',$min_page);
    $smarty->assign('max_page',$max_page);
    $smarty->assign('list_of_emails',$emailsList);
    $smarty->assign("error", '');
    $type = $_REQUEST['type'];
    #var_dump($page.' - '.$limit);
    #var_dump($emailsList);
    #die();
    $start = 1;
    $smarty->assign('start',$start);
    $smarty->display($tpl_folder.'/admin-1000/emails_management.tpl');

}

if ($action =='admin-settings'){
    if (!$is_admin)
    {

        header("Location: $root_url_admin");
        die();
    }

    $Settings = new Settings();
    $FormatDate = new FormatDate();

    $where = array();
    $get_data = array();
    $get_data['s_name'] = $Sanitation->remove_html($_REQUEST['s_name'], true);

    if ($get_data['s_name'] != '') {
        $where[] = " AND setting_name LIKE '%".$Sanitation->prepare_for_database($get_data['s_name'])."%' " ;
        $searched = true;
    }

    $smarty->assign('searched',$searched);
    $smarty->assign('s_name',$get_data['s_name']);


    $allSettings_db = $Settings -> fetch_settings(false, false,'setting_name','asc', implode(' ', $where));

    $allSettings = array();
    foreach ($allSettings_db as $key => $setting)
    {
        $allSettings[$key] = $setting;
        $allSettings[$key]['creation_date'] =  $FormatDate -> long_format($setting['creation_date']);
        $allSettings[$key]['last_update_date'] = ($category['last_update_date'] != '0000-00-00 00:00:00')? $FormatDate -> long_format($setting['last_update_date']) : $FormatDate -> long_format($setting['creation_date']);

    }

    $smarty->assign('allSettings',$allSettings);
    $smarty->assign("error", '');

    $smarty->display($tpl_folder_admin.'/admin_settings.tpl');
}
  
