<?php

/**
 * Created by JetBrains PhpStorm.
 * User: razvan
 * Date: 17/01/2013
 * Time: 10:10 AM
 * To change this template use File | Settings | File Templates.
 */


class loginAdmin
{
    protected $pending_widthdrawals;

    public function __construct()
    {
    }


    public function check_auth_state()
    {
        global $smarty;

        #var_dump ($smarty);
        #die('df');

        if ((!isset($_SESSION['user_id'])) || ($_SESSION['user_id'] == 0))
        {
            // Set some reasonable defaults;
            $this->set_session_defaults();
            return false;
        }
        #var_dump($_SESSION);
        #die();
        $smarty->assign('logged_user_id', $_SESSION['user_id']);
        $smarty->assign('logged_user_name', $_SESSION['user_name']);
        $smarty->assign('logged_user_type_id', $_SESSION['user_type']);
        $smarty->assign('logged_user_email', $_SESSION['email']);
        return true;
    }


    protected function set_session_defaults()
    {
        $_SESSION['logged'] = false;
        $_SESSION['user_id'] = 0;
        $_SESSION['user_name'] = '';
        $_SESSION['user_type'] = 0;
        $_SESSION['email'] = '';
    }


    public function login_user($email, $pass)
    {
        global $db_connection, $smarty, $tpl_folder_admin, $tpl_folder_admin, $tpl_folder;
        #echo "email = '$email', pass = '$pass'\n";
       // die();

        #$smarty->assign('tpl_folder', $tpl_folder);
        $smarty->assign('tpl_folder', $tpl_folder_admin);
        $smarty->assign('tpl_folder_admin', $tpl_folder_admin);
        #$smarty->assign('tpl_folder_admin', $tpl_folder_admin);
        #$smarty->assign('tpl_folder_root', $tpl_folder_root);

        if (($email == '') && ($pass == ''))
        {
            // We must display the login form;
            #$smarty->display($tpl_folder_admin.'/index-admin.tpl');
            // We return false, because at this point the user is not logged;
            return false;
        }

        // Else, we must attempt to login the user with the supplied credentials;
        $email_safe_4db = $db_connection->qstr($email, get_magic_quotes_gpc());

        $sql = "
            # File: " . __FILE__ . "
            # Line: " . __LINE__ . "
            SELECT
                user_id,
                user_name,
                user_type,
                password
            FROM
                imgup_users
            WHERE
                email = {$email_safe_4db}
            LIMIT 1;";
        var_dump ("sql = '$sql' <br />\n");
        $rs = $db_connection->execute($sql);
        $pass_data = $rs->getrows();

        $user_id = $pass_data [0] ['user_id'];
        $user_name = $pass_data [0] ['user_name'];
        $user_type = $pass_data [0] ['user_type'];
        $pass_hash = $pass_data [0] ['password'];

        $bcrypt = new Bcrypt();
        $is_good = $bcrypt->verify($pass, $pass_hash);
       // echo "pass = '$pass' <br />\n";
       // echo 'pass_hash = $2a$12$g8UsjKNZIxjCkpQUBaAsFOHgmt0/fkfoY6gc7V6q2kfzGGMv9Ph4K'. "<br />\n";
      # echo "pass_hash = '$pass_hash' <br />\n";

      //  echo "is_good = '$is_good' <br />\n"; die();
        if ($is_good)
        {
            $_SESSION['logged'] = true;
            $_SESSION['user_id'] = $user_id;
            $_SESSION['user_name'] = $user_name;
            $_SESSION['user_type'] = $user_type;
            $_SESSION['email'] = $email;

            $smarty->assign('logged_user_id', $_SESSION['user_id']);
            $smarty->assign('logged_user_name', $_SESSION['user_name']);
            $smarty->assign('logged_user_type_id', $_SESSION['user_type']);
            $smarty->assign('logged_user_email', $_SESSION['email']);

            return true;
        }
        else
        {
            // Authentication has failed!
            $smarty->assign('user_email', $_SESSION['email']);
            $smarty->assign('error', "Incorrect email or password. Please try again.");
           // $smarty->display($tpl_folder.'/index-admin.tpl');
            return false;
        }
    }


    public function logout()
    {
        global $config, $root_url_admin;

        $base_url = $config['BASE_URL'];

        $this->set_session_defaults();
        header("Location: {$root_url_admin}/");
        exit();
    }
}
