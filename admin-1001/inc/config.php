<?php

require(dirname(dirname(dirname(__FILE__))) . '/inc/config.php');

// Remove this in production!
ini_set("display_errors", 1);
error_reporting(E_ALL ^ E_NOTICE);

$config['FILE_MAX_FILE_SIZE'] = '20000';// 1 Mb
$config['IMAGES_PATH_DIR'] = $config['BASE_URL'].'/img';
$tpl_folder = $config['BASE_DIR'] . 'tpl/';
$tpl_folder_admin = $config['BASE_DIR'] . '/tpl/admin-1000';
$tpl_folder_root = $config['BASE_DIR'].'tpl/';
$root_url_admin = $config['BASE_URL'].'admin-1001';


/*Paginations variables*/
$config['CT_NUMBER_OF_USERS_PER_PAGE'] = 10; // rows per page
$config['CT_NUMBER_OF_ARTICLES_PER_PAGE'] = 10; // rows per page
$config['CT_NUMBER_OF_ASSETS_PER_PAGE'] = 10; // rows per page
$config['CT_NUMBER_OF_EMAILS_PER_PAGE'] = 10; // rows per page
$config['CT__NUMBER_OF_ERRORS_PER_PAGE'] = 10; // rows per page

//LOAD CLASSES
require($config['BASE_DIR'] . '/inc/FormatDate.class.php');
require($config['BASE_DIR'] . '/inc/Site_settings.class.php');
require $config['BASE_DIR'] . '/inc/ErrorHandler.php';
require $config['BASE_DIR'] . '/inc/bcrypt.class.php';
require($config['BASE_DIR'] . '/admin-1001/inc/Users.class.php');
require $config['BASE_DIR'] . '/admin-1001/inc/loginAdmin.class.php';
require($config['BASE_DIR'] . '/inc/emails_management.class.php');
require($config['BASE_DIR'] . '/inc/Settings.class.php');



//pages
$login_url = $root_url . '/?action=login';
$logout_url =  $root_url . '/?action=logout';
$home_url = $config['BASE_URL'] . '';

$smarty->assign('tpl_folder_root', $tpl_folder_root);
$smarty->assign('tpl_folder', $tpl_folder);
$smarty->assign("root_url_admin", $root_url_admin);

$admin_login_url =  $root_url_admin.'/?action=admin-login';
$admin_new_user_url = $root_url_admin.'/?action=admin-add_user';
$admin_new_admin_url = $root_url_admin.'/?action=admin-add_admin';
$admin_edit_user_url = $root_url_admin.'/?action=admin-edit_user';
$admin_users_url = $root_url_admin.'/?action=admin-users';
$admin_users_logout_url = $root_url_admin.'/?action=admin-logout';
$admin_edit_user_url = $root_url_admin.'/?action=admin-edit_user';
$admin_edit_admin_url = $root_url_admin.'/?action=admin-edit_admin';
$admin_users_front_url = $root_url_admin.'/?action=admin-front-users';
$admin_errors_url = $root_url_admin.'/?action=admin-errors';
$admin_articles_url = $root_url_admin.'/?action=admin-articles';
$admin_articles_add_url = $root_url_admin.'/?action=admin-articles-add';
$admin_articles_edit_url = $root_url_admin.'/?action=admin-articles-edit';
$admin_articles_delete_url = $root_url_admin.'/?action=admin-articles-delete';
$admin_assets_url = $root_url_admin.'/?action=admin-assets';
$admin_assets_add_url = $root_url_admin.'/?action=admin-assets-add';
$admin_assets_edit_url = $root_url_admin.'/?action=admin-assets-edit';
$admin_assets_delete_url = $root_url_admin.'/?action=admin-assets-delete';
$admin_categories_url = $root_url_admin.'/?action=admin-categories';
$admin_comments_unmoderated_url = $root_url_admin.'/?action=admin-comments_unmoderated';
$admin_comments_all_url = $root_url_admin.'/?action=admin-comments';

//Admin section
$smarty->assign('admin_login_url',$admin_login_url );
$smarty->assign('admin_new_user_url',$admin_new_user_url );
$smarty->assign('admin_edit_user_url',$admin_edit_user_url );

$smarty->assign('admin_new_admin_url',$admin_new_admin_url );
$smarty->assign('admin_edit_admin_url',$admin_edit_admin_url );

$smarty->assign('admin_users_url',$admin_users_url );
$smarty->assign('admin_users_logout_url',$admin_users_logout_url );
$smarty->assign('admin_users_front_url',$admin_users_front_url );
$smarty->assign('admin_errors_url',$admin_errors_url );

$smarty->assign('admin_edit_user_url',$admin_edit_user_url );
$smarty->assign('admin_edit_admin_url',$admin_edit_admin_url );
$smarty->assign('admin_articles_url',$admin_articles_url );
$smarty->assign('admin_articles_add_url',$admin_articles_add_url );
$smarty->assign('admin_articles_edit_url',$admin_articles_edit_url );
$smarty->assign('admin_articles_delete_url',$admin_articles_delete_url );

$smarty->assign('admin_assets_url',$admin_assets_url );
$smarty->assign('admin_assets_add_url',$admin_assets_add_url );
$smarty->assign('admin_assets_edit_url',$admin_assets_edit_url );
$smarty->assign('admin_assets_delete_url',$admin_assets_delete_url );
$smarty->assign('ASSETS_PATH_DIR',$config['ASSETS_PATH_DIR'] );
$smarty->assign('IMAGES_PATH_DIR',$config['IMAGES_PATH_DIR'] );

$smarty->assign('admin_categories_url',$admin_categories_url );

$smarty->assign('admin_comments_unmoderated_url',$admin_comments_unmoderated_url );
$smarty->assign('admin_comments_all_url',$admin_comments_all_url );


$admin_emails_management_url = $root_url_admin.'/?action=admin-emails_management';
$smarty->assign('admin_emails_management_url',$admin_emails_management_url );

$admin_settings_url = $root_url_admin.'/?action=admin-settings';
$smarty->assign('admin_settings_url',$admin_settings_url );



/**
 * Convert number of seconds into hours, minutes and seconds
 * and return an array containing those values
 *
 * @param integer $inputSeconds Number of seconds to parse
 * @return array
 */
function secondsToTimeAdmin($inputSeconds) {

    $secondsInAMinute = 60;
    $secondsInAnHour  = 60 * $secondsInAMinute;
    $secondsInADay    = 24 * $secondsInAnHour;

    // extract days
    $days = floor($inputSeconds / $secondsInADay);

    // extract hours
    $hourSeconds = $inputSeconds % $secondsInADay;
    $hours = floor($hourSeconds / $secondsInAnHour);

    // extract minutes
    $minuteSeconds = $hourSeconds % $secondsInAnHour;
    $minutes = floor($minuteSeconds / $secondsInAMinute);

    // extract the remaining seconds
    $remainingSeconds = $minuteSeconds % $secondsInAMinute;
    $seconds = ceil($remainingSeconds);

    // return the final array
    $obj = array(
        'd' => (int) $days,
        'h' => (int) $hours,
        'm' => (int) $minutes,
        's' => (int) $seconds,
    );
    return $obj;
}
